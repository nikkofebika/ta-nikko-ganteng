<?php 
session_start();
include 'include/config.php';

$id_customer = $_SESSION['pelanggan']['id_customer'];

if (empty($_SESSION['pelanggan']) || !isset($_SESSION['pelanggan'])) {
  echo "<script>alert('Anda Belum Login. Silahkan Login Dulu');</script>";
  echo "<script>location='index.php';</script>";
}

//QUERY UNTUK MENDAPATKAN DATA LAPANGAN
                  // $q = $con->query("SELECT id_customer, nama_lapangan FROM lapangan WHERE id_customer='$id_customer'");
                  // $ambil = $q->fetch_assoc();
                  // $idlap = $ambil['id_customer'];
                  // $nmlap = $ambil['nama_lapangan'];


?>
<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Theme Made By www.w3schools.com - No Copyright -->
  <title>BINTANG FUTSAL</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link href="css/font.css" rel="stylesheet" type="text/css">
  <script src="js/jquery-1.11.1.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <style>
    body {
      font: 400 15px/1.8 Lato, sans-serif;
      color: #777;
    }
    h3, h4 {
      margin: 10px 0 30px 0;
      letter-spacing: 10px;      
      font-size: 20px;
      color: #111;
    }
    .container {
      padding: 80px 120px;
    }
    .person {
      border: 10px solid transparent;
      margin-bottom: 25px;
      width: 80%;
      height: 80%;
      opacity: 0.7;
    }
    .person:hover {
      border-color: #f1f1f1;
    }
    .carousel-inner img {
      -webkit-filter: grayscale(90%);
      filter: grayscale(90%); /* make all photos black and white */ 
      width: 100%; /* Set width to 100% */
      margin: auto;
    }
    .carousel-caption h3 {
      color: #fff !important;
    }
    @media (max-width: 600px) {
      .carousel-caption {
        display: none; /* Hide the carousel text when the screen is less than 600 pixels wide */
      }
    }
    .bg-1 {
      background: #2d2d30;
      color: #bdbdbd;
    }
    .bg-1 h3 {color: #fff;}
    .bg-1 p {font-style: italic;}
    .list-group-item:first-child {
      border-top-right-radius: 0;
      border-top-left-radius: 0;
    }
    .list-group-item:last-child {
      border-bottom-right-radius: 0;
      border-bottom-left-radius: 0;
    }
    .thumbnail {
      padding: 0 0 15px 0;
      border: none;
      border-radius: 0;
    }
    .thumbnail p {
      margin-top: 15px;
      color: #555;
    }

    .modal-header, h4, .close {
      background-color: #333;
      color: #fff !important;
      text-align: center;
      font-size: 30px;
    }
    .modal-body {
      padding: 40px 50px;
    }
    .nav-tabs li a {
      color: #777;
    }
    #googleMap {
      width: 100%;
      height: 400px;
      -webkit-filter: grayscale(100%);
      filter: grayscale(100%);
    }  
    .navbar {
      font-family: Montserrat, sans-serif;
      margin-bottom: 0;
      background-color: #2d2d30;
      border: 0;
      font-size: 11px !important;
      letter-spacing: 4px;
      opacity: 0.9;
    }
    .navbar li a, .navbar .navbar-brand { 
      color: #d5d5d5 !important;
    }
    .navbar-nav li a:hover {
      color: #fff !important;
    }
    .navbar-nav li.active a {
      color: #fff !important;
      background-color: #29292c !important;
    }
    .navbar-default .navbar-toggle {
      border-color: transparent;
    }
    .open .dropdown-toggle {
      color: #fff;
      background-color: #555 !important;
    }
    .dropdown-menu li a {
      color: #000 !important;
    }
    .dropdown-menu li a:hover {
      background-color: red !important;
    }
    footer {
      background-color: #2d2d30;
      color: #f5f5f5;
      padding: 32px;
    }
    footer a {
      color: #f5f5f5;
    }
    footer a:hover {
      color: #777;
      text-decoration: none;
    }  
    .form-control {
      border-radius: 0;
    }
    textarea {
      resize: none;
    }
  </style>
</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="50">
  <nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>                        
        </button>
        <a class="navbar-brand" href="#myPage"><img src="foto/logo-lap-futsal.jpg" alt="New York" width="60" height="auto"></a>
      </div>
      <div class="collapse navbar-collapse" id="myNavbar">
        <ul class="nav navbar-nav navbar-right">
          <li><a href="index.php">HOME</a></li>

          <?php if (isset($_SESSION['pelanggan'])): ?>
            <li><a href="riwayat.php">RIWAYAT BOOKING</a></li>
            <li><a href="logout.php">LOGOUT</a></li>
            <!-- jika tidak ada session pelanggan -->
            <?php else: ?>
              <li><a data-toggle="modal" data-target="#login">LOGIN</a></li>
              <li><a data-toggle="modal" data-target="#register">REGISTER</a></li>
            <?php endif ?>


          </ul>
        </div>
      </div>
    </nav>
    <section class="lapangan">
      <div class="container" style="padding: 80px 0;">
        <h2 class="text-center">Daftar Booking <?php echo ucfirst($_SESSION['pelanggan']['username']) ?></h2>
        <br>
        <table class="table table-bordered table-striped table-hover">
          <thead>
            <tr>
              <th class="text-center">No</th>
              <th class="text-center">Nama Lapangan</th>
              <th class="text-center">Tanggal Main</th>
              <th class="text-center">Jam Mulai</th>
              <th class="text-center">Jam Selesai</th>
              <th class="text-center">Uang Muka</th>
              <th class="text-center">Total Bayar</th>
              <th class="text-center">Status Uang Muka</th>
              <!-- <th class="text-center">Main</th> -->
              <th class="text-center">Opsi</th>
            </tr>
          </thead>
          <tbody>
            <?php
            $no=1;
            $query = "SELECT lapangan.nama_lapangan, lapangan.id_lapangan, list_booking.tgl_main, list_booking.jam_mulai, list_booking.jam_selesai, list_booking.dp, list_booking.total_harga, list_booking.status_dp, list_booking.id_booking 
            FROM 
            list_booking 
            JOIN lapangan ON lapangan.id_lapangan=list_booking.id_lapangan
            WHERE list_booking.id_customer='$id_customer' ORDER BY list_booking.id_booking DESC";
            $ambil = $con->query($query);
            while ($booking = $ambil->fetch_array()) {
              ?>
              <tr>
                <td><?php echo $no++; ?></td>
                <td><?php echo $booking['nama_lapangan']; ?></td>
                <td><?php echo $booking['tgl_main']; ?></td>
                <td><?php echo $booking['jam_mulai']; ?></td>
                <td><?php echo $booking['jam_selesai']; ?></td>
                <td><?php echo $booking['dp']; ?></td>
                <td><?php echo $booking['total_harga']; ?></td>
                <td><?php echo $booking['status_dp']; ?></td>
                <td>
                 <a href="nota.php?id_booking=<?php echo $booking['id_booking'] ?>" class="btn btn-primary">Nota</a>
                 <?php if ($booking['status_dp'] == 'Belum Dibayar'): ?>
                   <a href="pembayaran.php?id_booking=<?php echo $booking['id_booking'] ?>" class="btn btn-warning">Bayar</a>
                 <?php endif ?>
               </td>
             </tr>
             <?php
           }
           ?>
         </tbody>
       </table>
     </div>
   </section>
 </body>
 </html>
