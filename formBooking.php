<?php 
session_start();
include 'include/config.php';

$id_lapangan = $_GET['id'];

if (empty($_SESSION['pelanggan']) || !isset($_SESSION['pelanggan'])) {
  echo "<script>alert('Anda Belum Login. Silahkan Login Dulu');</script>";
  echo "<script>location='index.php';</script>";
}

//QUERY UNTUK MENDAPATKAN DATA LAPANGAN
$q = $con->query("SELECT id_lapangan, nama_lapangan FROM lapangan WHERE id_lapangan='$id_lapangan'");
$ambil = $q->fetch_assoc();
$idlap = $ambil['id_lapangan'];
$nmlap = $ambil['nama_lapangan'];


?>
<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Theme Made By www.w3schools.com - No Copyright -->
  <title>BINTANG FUTSAL</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link href="css/font.css" rel="stylesheet" type="text/css">
  <script src="js/jquery-1.11.1.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <style>
    body {
      font: 400 15px/1.8 Lato, sans-serif;
      color: #777;
    }
    h3, h4 {
      margin: 10px 0 30px 0;
      letter-spacing: 10px;      
      font-size: 20px;
      color: #111;
    }
    .container {
      padding: 80px 120px;
    }
    .person {
      border: 10px solid transparent;
      margin-bottom: 25px;
      width: 80%;
      height: 80%;
      opacity: 0.7;
    }
    .person:hover {
      border-color: #f1f1f1;
    }
    .carousel-inner img {
      -webkit-filter: grayscale(90%);
      filter: grayscale(90%); /* make all photos black and white */ 
      width: 100%; /* Set width to 100% */
      margin: auto;
    }
    .carousel-caption h3 {
      color: #fff !important;
    }
    @media (max-width: 600px) {
      .carousel-caption {
        display: none; /* Hide the carousel text when the screen is less than 600 pixels wide */
      }
    }
    .bg-1 {
      background: #2d2d30;
      color: #bdbdbd;
    }
    .bg-1 h3 {color: #fff;}
    .bg-1 p {font-style: italic;}
    .list-group-item:first-child {
      border-top-right-radius: 0;
      border-top-left-radius: 0;
    }
    .list-group-item:last-child {
      border-bottom-right-radius: 0;
      border-bottom-left-radius: 0;
    }
    .thumbnail {
      padding: 0 0 15px 0;
      border: none;
      border-radius: 0;
    }
    .thumbnail p {
      margin-top: 15px;
      color: #555;
    }

    .modal-header, h4, .close {
      background-color: #333;
      color: #fff !important;
      text-align: center;
      font-size: 30px;
    }
    .modal-body {
      padding: 40px 50px;
    }
    .nav-tabs li a {
      color: #777;
    }
    #googleMap {
      width: 100%;
      height: 400px;
      -webkit-filter: grayscale(100%);
      filter: grayscale(100%);
    }  
    .navbar {
      font-family: Montserrat, sans-serif;
      margin-bottom: 0;
      background-color: #2d2d30;
      border: 0;
      font-size: 11px !important;
      letter-spacing: 4px;
      opacity: 0.9;
    }
    .navbar li a, .navbar .navbar-brand { 
      color: #d5d5d5 !important;
    }
    .navbar-nav li a:hover {
      color: #fff !important;
    }
    .navbar-nav li.active a {
      color: #fff !important;
      background-color: #29292c !important;
    }
    .navbar-default .navbar-toggle {
      border-color: transparent;
    }
    .open .dropdown-toggle {
      color: #fff;
      background-color: #555 !important;
    }
    .dropdown-menu li a {
      color: #000 !important;
    }
    .dropdown-menu li a:hover {
      background-color: red !important;
    }
    footer {
      background-color: #2d2d30;
      color: #f5f5f5;
      padding: 32px;
    }
    footer a {
      color: #f5f5f5;
    }
    footer a:hover {
      color: #777;
      text-decoration: none;
    }  
    .form-control {
      border-radius: 0;
    }
    textarea {
      resize: none;
    }
  </style>
</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="50">
  <nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>                        
        </button>
        <a class="navbar-brand" href="#myPage"><img src="foto/logo-lap-futsal.jpg" alt="New York" width="60" height="auto"></a>
      </div>
      <div class="collapse navbar-collapse" id="myNavbar">
        <ul class="nav navbar-nav navbar-right">
          <li><a href="index.php">HOME</a></li>

          <?php if (isset($_SESSION['pelanggan'])): ?>
            <li><a href="riwayatbooking.php">RIWAYAT BOOKING</a></li>
            <li><a href="logout.php">LOGOUT</a></li>
            <!-- jika tidak ada session pelanggan -->
            <?php else: ?>
              <li><a data-toggle="modal" data-target="#login">LOGIN</a></li>
              <li><a data-toggle="modal" data-target="#register">REGISTER</a></li>
            <?php endif ?>


          </ul>
        </div>
      </div>
    </nav>
    <section class="lapangan">
      <div class="container">
        <h2 class="text-center">Form booking <?php echo $nmlap ?></h2>
        <br>
        <form role="form" method="post" action="saveBooking.php">
              <input type="hidden" name="id_customer" value="<?php echo $_SESSION['pelanggan']['id_customer'] ?> ">
              <input type="hidden" name="id_lapangan" value="<?php echo $id_lapangan ?> ">
              
              <div class="form-group">
                <input type="hidden" readonly class="form-control" name="id_lapangan" value="<?php echo $idlap ?>">
              </div>
              <div class="form-group">
                <label>Jam Mulai</label>
                <select name="jam_mulai" class="form-control" required> 
                  <option value="">-- Pilih Jam --</option>
                  <?php $ambil=$con->query("SELECT * FROM jam");
                  while($jam=$ambil->fetch_assoc()) { ?>
                    <option value="<?php echo $jam['jam']?>"><?php echo $jam['jam']?></option>
                  <?php } ?>
                </select>
              </div>
              <div class="form-group">
                <label>Jam Selesai</label>
                <select name="jam_selesai" class="form-control" required> 
                  <option value="">-- Pilih Jam --</option>
                  <?php $ambil=$con->query("SELECT * FROM jam");
                  while($jam=$ambil->fetch_assoc()) { ?>
                    <option value="<?php echo $jam['jam']?>"><?php echo $jam['jam']?></option>
                  <?php } ?>
                </select>
              </div>
              <div class="form-group">
                <label>Tanggal Main</label>
                <input type="date" name="tgl_main" required class="form-control">
              </div>
              <center>
                <button name="booking" class="btn btn-primary">Booking</button>
                <a onclick="history.go(-1);return false;" class="btn btn-danger">Cancel</a>
              </center>
            </form>
      </div>
    </section>

    <!-- PROSES BOOKING FRONT-END -->
    <?php
    // if (isset($_POST['booking'])) {
    //   // echo "<pre>";
    //   // print_r($_POST);
    //   // echo "</pre><br>";die;

    //   $id_customer = $_POST['id_customer'];
    //   $id_lapangan = $_POST['id_lapangan'];
    //   $jam_mulai = $_POST['jam_mulai']; 
    //   $jam_selesai = $_POST['jam_selesai'];
    //   $tgl_booking = date("Y-m-d");
    //   $tgl_main = $_POST['tgl_main'];

      

    //   $sql = $con->query("SELECT RIGHT(id_booking, 3) as kode FROM list_booking ORDER BY id_booking DESC LIMIT 1");

    //   $hitung = mysqli_num_rows($sql);

    //   if ($hitung > 0) {
    //     $q = $sql->fetch_assoc();
    //     $kode = $q['kode']+1;
    //     // print_r($kode);die;
    //   }else{
    //     $kode = 1;
    //   }
    //   $code = "B".$id_lapangan.str_pad($kode, 3,"0", STR_PAD_LEFT);
    //   echo $code."<br>";
    // echo "<pre>";
    // print_r($_POST);
    // echo "</pre>"."<br>";


      
    //   // $dp = $_POST['dp'];

    //   $cek_tgl_main = strtotime($tgl_main);

    //   $jmmulai  = strtotime($jam_mulai)/3600;
    //   $jmselesai  = strtotime($jam_selesai)/3600;

    //   $lap = $con->query("SELECT * FROM lapangan WHERE id_lapangan='$id_lapangan'");
    //   $datalapangan = $lap->fetch_assoc();
    //   $hargalapangan = $datalapangan['harga'];

    //   $totaljammain = $jmselesai - $jmmulai;
    //   $total_harga = $totaljammain * $hargalapangan;
    //   $totaldp = $total_harga / 2;
    //   // echo $totaldp;die;
    //             // if ($total_harga == $dp) {
    //             //   $status = "lunas";
    //             // }
    //             // else{
    //             //   $status = "belum_lunas";
    //             // }

    //            /* $totaljammain = $jam_selesai - $jam_mulai;
    //             $total_harga = $totaljammain * $hargalapangan;
    //             */
    //             if($tgl_main < $tgl_booking && $jmselesai < $jmmulai){
    //               echo "<script>alert('Tanggal & Jam Main tidak boleh sebelum Tanggal & Jam booking');window.location='listbooking.php?id=$id_lapangan'</script>";
    //             }elseif($tgl_main < $tgl_booking ){
    //               echo "<script>alert('Tanggal Main tidak boleh sebelum Tanggal Booking');window.location='listbooking.php?id=$id_lapangan'</script>";
    //             }elseif($jmselesai < $jmmulai ){
    //               echo "<script>alert('Jam Selesai tidak boleh sebelum Jam Mulai');window.location='listbooking.php?id=$id_lapangan'</script>";
    //             }elseif($jmselesai == $jmmulai ){
    //               echo "<script>alert('Jam Mulai Dan Jam Selesai Tidak Boleh Sama');window.location='listbooking.php?id=$id_lapangan'</script>";
    //             }else{
    //               $con->query("INSERT INTO list_booking (id_booking,id_customer, id_lapangan, jam_mulai, jam_selesai, tgl_booking, tgl_main, dp, total_harga) VALUES ('$code','$id_customer','$id_lapangan','$jam_mulai','$jam_selesai','$tgl_booking','$tgl_main','$totaldp','$total_harga')");
    //            }
    //            if ($con->affected_rows > 0){
    //             echo "<script>alert('Data Booking telah berhasil disimpan');window.location='riwayatbooking.php?id_customer=$id_customer'</script>";
    //           }
    //         }

            ?>
            <!-- /PROSES BOOKING FRONT-END -->


          </body>
          </html>
