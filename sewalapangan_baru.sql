-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 04, 2019 at 04:50 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sewalapangannn`
--

-- --------------------------------------------------------

--
-- Table structure for table `bank`
--

CREATE TABLE `bank` (
  `id_bank` int(11) NOT NULL,
  `nama_bank` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bank`
--

INSERT INTO `bank` (`id_bank`, `nama_bank`) VALUES
(1, 'BCA'),
(2, 'BNI'),
(3, 'Mandiri'),
(4, 'BRI'),
(5, 'BJB'),
(6, 'Bank Banten'),
(7, 'CIMB Niaga'),
(8, 'Bukopin'),
(9, 'Mayora');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id_customer` varchar(10) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `nama_lengkap` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `no_telp` varchar(15) NOT NULL,
  `alamat` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id_customer`, `username`, `password`, `nama_lengkap`, `email`, `no_telp`, `alamat`) VALUES
('CST001', 'Nikko', 'bbb1818a0e674a0a8c3b0ac266f91313', 'Nikko Febika', 'febika.nikko@gmail.com', '081519364336', 'Graha Lestari, Cikupa Tangerang'),
('CST002', 'Dendi', '9d47cb51d31cc4adbdaa29cde5070c7c', 'Dendi Kurniawan', 'dendi@gmail.com', '087791873674', 'Cimone Tangereang'),
('CST003', 'Arum', 'b41eff24b37ec9e4fe4dcf968da2a8ba', 'Arum Paujiah', 'arum@gmail.com', '085144587723', 'Kp. Kelapa Balaraja Tangerang');

-- --------------------------------------------------------

--
-- Table structure for table `jam`
--

CREATE TABLE `jam` (
  `id_jam` int(11) NOT NULL,
  `jam` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jam`
--

INSERT INTO `jam` (`id_jam`, `jam`) VALUES
(15, '08:00:00'),
(16, '09:00:00'),
(17, '10:00:00'),
(18, '11:00:00'),
(19, '12:00:00'),
(20, '13:00:00'),
(21, '14:00:00'),
(22, '15:00:00'),
(23, '16:00:00'),
(24, '17:00:00'),
(25, '18:00:00'),
(26, '19:00:00'),
(27, '20:00:00'),
(28, '21:00:00'),
(29, '22:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `lapangan`
--

CREATE TABLE `lapangan` (
  `id_lapangan` varchar(10) NOT NULL,
  `size_lapangan` enum('large','small','','') NOT NULL,
  `harga` int(11) NOT NULL,
  `nama_lapangan` varchar(50) NOT NULL,
  `keterangan` text NOT NULL,
  `picture` varchar(50) NOT NULL DEFAULT 'default.jpg',
  `fasilitas` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lapangan`
--

INSERT INTO `lapangan` (`id_lapangan`, `size_lapangan`, `harga`, `nama_lapangan`, `keterangan`, `picture`, `fasilitas`) VALUES
('LAP1', 'large', 90000, 'Lapangan 1', 'Lapangan ukuran besar dengan alas polyethyle atau karpet plastik', 'lapangan1.jpg', 'lapangan lebar,free air mineral,alas plastik baru'),
('LAP2', 'small', 80000, 'Lapangan 2', 'Lapangan ukuran kecil dengan alas rumput sintetis', 'lap-futsal2.jpg', 'Rumput sintetis,free air mineral');

-- --------------------------------------------------------

--
-- Table structure for table `list_booking`
--

CREATE TABLE `list_booking` (
  `id_booking` varchar(10) NOT NULL,
  `id_customer` varchar(10) NOT NULL,
  `id_lapangan` varchar(10) NOT NULL,
  `jam_mulai` time NOT NULL,
  `jam_selesai` time NOT NULL,
  `tgl_booking` date NOT NULL,
  `tgl_main` date NOT NULL,
  `dp` int(11) NOT NULL,
  `status_dp` varchar(25) NOT NULL DEFAULT 'Belum Dibayar',
  `total_harga` int(11) NOT NULL,
  `status` varchar(25) DEFAULT 'Belum Main'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `list_booking`
--

INSERT INTO `list_booking` (`id_booking`, `id_customer`, `id_lapangan`, `jam_mulai`, `jam_selesai`, `tgl_booking`, `tgl_main`, `dp`, `status_dp`, `total_harga`, `status`) VALUES
('BLAP1001', 'CST002 ', 'LAP1', '08:00:00', '09:00:00', '2019-07-04', '2019-07-05', 45000, 'Sedang Diproses', 90000, 'Belum Main'),
('BLAP1002', 'CST002 ', 'LAP1', '08:00:00', '09:00:00', '2019-07-04', '2019-07-05', 45000, 'Belum Dibayar', 90000, 'Belum Main'),
('BLAP1003', 'CST002 ', 'LAP1', '08:00:00', '09:00:00', '2019-07-04', '2019-07-05', 45000, 'Belum Dibayar', 90000, 'Belum Main'),
('BLAP1006', 'CST001 ', 'LAP1', '13:00:00', '15:00:00', '2019-07-04', '2019-07-18', 90000, 'Belum Dibayar', 180000, 'Belum Main'),
('BLAP2004', 'CST001 ', 'LAP2', '08:00:00', '17:00:00', '2019-07-04', '2019-07-12', 360000, 'Sedang Diproses', 720000, 'Belum Main'),
('BLAP2005', 'CST001 ', 'LAP2', '09:00:00', '12:00:00', '2019-07-04', '2019-07-09', 120000, 'Sedang Diproses', 240000, 'Belum Main'),
('BLAP2006', 'CST001 ', 'LAP2', '08:00:00', '15:00:00', '2019-07-04', '2019-07-13', 280000, 'Sedang Diproses', 560000, 'Belum Main'),
('BLAP2007', 'CST002 ', 'LAP2', '13:00:00', '22:00:00', '2019-07-04', '2019-07-18', 360000, 'Sedang Diproses', 720000, 'Belum Main'),
('BLAP2008', 'CST002 ', 'LAP2', '21:00:00', '22:00:00', '2019-07-04', '2019-07-05', 40000, 'Lunas', 80000, 'Belum Main');

-- --------------------------------------------------------

--
-- Table structure for table `pembayaran`
--

CREATE TABLE `pembayaran` (
  `id_booking` varchar(10) NOT NULL,
  `id_pembayaran` varchar(15) NOT NULL,
  `tgl_pembayaran` date DEFAULT NULL,
  `uang_muka` int(11) NOT NULL,
  `total_pembayaran` int(11) NOT NULL,
  `nama_penyetor` varchar(50) NOT NULL,
  `id_bank` int(11) DEFAULT NULL,
  `tgl_transfer` date DEFAULT NULL,
  `bukti_transfer` varchar(50) DEFAULT NULL,
  `status` char(1) DEFAULT '0' COMMENT '0=baru transfer, 1=sudah lunas'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembayaran`
--

INSERT INTO `pembayaran` (`id_booking`, `id_pembayaran`, `tgl_pembayaran`, `uang_muka`, `total_pembayaran`, `nama_penyetor`, `id_bank`, `tgl_transfer`, `bukti_transfer`, `status`) VALUES
('BLAP2006', 'PB0407201901', NULL, 280000, 560000, 'nikko', 8, '2019-07-04', '20190704031229admin.png', '0'),
('BLAP2007', 'PB0407201902', NULL, 360000, 720000, 'nikko', 0, '2019-07-04', '20190704033343admin.png', '0'),
('BLAP2008', 'PB0407201903', NULL, 40000, 80000, 'aliya', 6, '2019-07-04', '20190704033641admin.png', '0');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` varchar(10) NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `picture` varchar(50) NOT NULL DEFAULT 'default.jpg'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `username`, `email`, `password`, `picture`) VALUES
('ADM01', 'admin', 'admin@gmail.com', '21232f297a57a5a743894a0e4a801fc3', 'customer-service-icon.png');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bank`
--
ALTER TABLE `bank`
  ADD PRIMARY KEY (`id_bank`) USING BTREE;

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id_customer`);

--
-- Indexes for table `jam`
--
ALTER TABLE `jam`
  ADD PRIMARY KEY (`id_jam`);

--
-- Indexes for table `lapangan`
--
ALTER TABLE `lapangan`
  ADD PRIMARY KEY (`id_lapangan`);

--
-- Indexes for table `list_booking`
--
ALTER TABLE `list_booking`
  ADD PRIMARY KEY (`id_booking`);

--
-- Indexes for table `pembayaran`
--
ALTER TABLE `pembayaran`
  ADD PRIMARY KEY (`id_pembayaran`,`nama_penyetor`) USING BTREE;

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bank`
--
ALTER TABLE `bank`
  MODIFY `id_bank` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `jam`
--
ALTER TABLE `jam`
  MODIFY `id_jam` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
