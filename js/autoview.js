$(document).ready(function(){
	$('.txtNikCustomer').change(function(){
		var value = $('#formreservation').serialize();
		$.ajax({
			type : 'post',
			url : 'controler/textcustomer.php',
			data : value,
			dataType : 'json',
			success : function(data){
				$('#txtNameCustomer').val(data[0]);
			}
		});
	});
});

$(document).ready(function(){
	$('.txtIdRoom').change(function(){
		var value = $('#formreservation').serialize();
		$.ajax({
			type : 'post',
			url : 'controler/textroom.php',
			data : value,
			dataType : 'json',
			success : function(data){
				$('#txtType').val(data[0]);
				$('#txtCapacity').val(data[1]);
				$('#txtDescription').val(data[2]);
				$('#txtPrice').val(data[3]);
				$('#txtTotPrice').val(data[4]);
				$('#txtPPN').val(data[5]);
				$('#txtGrandtotal').val(data[6]);
			}
		});
	});
});

$(document).ready(function(){
	$('.cbDiscount').change(function(){
		var value = $('#formreservation').serialize();
		$.ajax({
			type : 'post',
			url : 'controler/textroom.php',
			data : value,
			dataType : 'json',
			success : function(data){
				$('#txtType').val(data[0]);
				$('#txtCapacity').val(data[1]);
				$('#txtDescription').val(data[2]);
				$('#txtPrice').val(data[3]);
				$('#txtTotPrice').val(data[4]);
				$('#txtPPN').val(data[5]);
				$('#txtGrandtotal').val(data[6]);
			}
		});
	});
});

$(document).ready(function(){
	$('.txtCheckout').change(function(){
		var value = $('#formreservation').serialize();
		$.ajax({
			type : 'post',
			url : 'controler/textroom.php',
			data : value,
			dataType : 'json',
			success : function(data){
				$('#txtType').val(data[0]);
				$('#txtCapacity').val(data[1]);
				$('#txtDescription').val(data[2]);
				$('#txtPrice').val(data[3]);
				$('#txtTotPrice').val(data[4]);
				$('#txtPPN').val(data[5]);
				$('#txtGrandtotal').val(data[6]);
			}
		});
	});
});
$(document).ready(function(){
	$('.txtCash').change(function(){
		var value = $('#formpayment').serialize();
		$.ajax({
			type : 'post',
			url : 'controler/textpayments.php',
			data : value,
			dataType : 'json',
			success : function(data){
				$('#txtChange').val(data[0]);
			}
		});
	});
});