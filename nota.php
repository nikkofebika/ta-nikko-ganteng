<?php 
session_start();
include 'include/config.php';

$id_booking = $_GET['id_booking'];
$id_customer = $_SESSION['pelanggan']['id_customer'];

if (empty($_SESSION['pelanggan']) || !isset($_SESSION['pelanggan'])) {
  echo "<script>alert('Anda Belum Login. Silahkan Login Dulu');</script>";
  echo "<script>location='index.php';</script>";
}

//QUERY UNTUK MENDAPATKAN DATA LIST BOOKING
$q = $con->query("SELECT * FROM list_booking JOIN lapangan ON lapangan.id_lapangan=list_booking.id_lapangan WHERE id_booking='$id_booking'");
$nota = $q->fetch_assoc();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Theme Made By www.w3schools.com - No Copyright -->
  <title>BINTANG FUTSAL</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link href="css/font-awesome.min.css" rel="stylesheet">
  <link href="css/font.css" rel="stylesheet" type="text/css">
  <script src="js/jquery-1.11.1.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <style>
    body {
      font: 400 15px/1.8 Lato, sans-serif;
      color: #777;
    }
    h3, h4 { 
      color: #111;
    }
    .container {
      padding: 80px 120px;
    }
    .person {
      border: 10px solid transparent;
      margin-bottom: 25px;
      width: 80%;
      height: 80%;
      opacity: 0.7;
    }
    .person:hover {
      border-color: #f1f1f1;
    }
    .carousel-inner img {
      -webkit-filter: grayscale(90%);
      filter: grayscale(90%); /* make all photos black and white */ 
      width: 100%; /* Set width to 100% */
      margin: auto;
    }
    .carousel-caption h3 {
      color: #fff !important;
    }
    @media (max-width: 600px) {
      .carousel-caption {
        display: none; /* Hide the carousel text when the screen is less than 600 pixels wide */
      }
    }
    .bg-1 {
      background: #2d2d30;
      color: #bdbdbd;
    }
    .bg-1 h3 {color: #fff;}
    .bg-1 p {font-style: italic;}
    .list-group-item:first-child {
      border-top-right-radius: 0;
      border-top-left-radius: 0;
    }
    .list-group-item:last-child {
      border-bottom-right-radius: 0;
      border-bottom-left-radius: 0;
    }
    .thumbnail {
      padding: 0 0 15px 0;
      border: none;
      border-radius: 0;
    }
    .thumbnail p {
      margin-top: 15px;
      color: #555;
    }

    .modal-header, h4, .close {
      background-color: #333;
      color: #fff !important;
      text-align: center;
      font-size: 30px;
    }
    .modal-body {
      padding: 40px 50px;
    }
    .nav-tabs li a {
      color: #777;
    }
    #googleMap {
      width: 100%;
      height: 400px;
      -webkit-filter: grayscale(100%);
      filter: grayscale(100%);
    }  
    .navbar {
      font-family: Montserrat, sans-serif;
      margin-bottom: 0;
      background-color: #2d2d30;
      border: 0;
      font-size: 11px !important;
      letter-spacing: 4px;
      opacity: 0.9;
    }
    .navbar li a, .navbar .navbar-brand { 
      color: #d5d5d5 !important;
    }
    .navbar-nav li a:hover {
      color: #fff !important;
    }
    .navbar-nav li.active a {
      color: #fff !important;
      background-color: #29292c !important;
    }
    .navbar-default .navbar-toggle {
      border-color: transparent;
    }
    .open .dropdown-toggle {
      color: #fff;
      background-color: #555 !important;
    }
    .dropdown-menu li a {
      color: #000 !important;
    }
    .dropdown-menu li a:hover {
      background-color: red !important;
    }
    footer {
      background-color: #2d2d30;
      color: #f5f5f5;
      padding: 32px;
    }
    footer a {
      color: #f5f5f5;
    }
    footer a:hover {
      color: #777;
      text-decoration: none;
    }  
    .form-control {
      border-radius: 0;
    }
    textarea {
      resize: none;
    }
</style>
</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="50">
  <nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>                        
        </button>
        <a class="navbar-brand" href="#myPage"><img src="foto/logo-lap-futsal.jpg" alt="New York" width="60" height="auto"></a>
      </div>
      <div class="collapse navbar-collapse" id="myNavbar">
        <ul class="nav navbar-nav navbar-right">
          <li><a href="index.php">HOME</a></li>

          <?php if (isset($_SESSION['pelanggan'])): ?>
            <li><a href="riwayatbooking.php">RIWAYAT BOOKING</a></li>
            <li><a href="logout.php">LOGOUT</a></li>
            <!-- jika tidak ada session pelanggan -->
            <?php else: ?>
              <li><a data-toggle="modal" data-target="#login">LOGIN</a></li>
              <li><a data-toggle="modal" data-target="#register">REGISTER</a></li>
            <?php endif ?>


          </ul>
        </div>
      </div>
  </nav>
    <section class="lapangan">
      <div class="container">
        <h2 class="text-center">Bukti Pemesanan Lapangan</h2>
        <br>
        <div class="row">
          <div class="col-lg-12 col-md-12">
            <div class="row">
              <div class="col-lg-6 col-md-6">
                <h3>Metode Pembayaran</h3>
                <div class="alert alert-info">Silahkan lakukan pembayaran <b>uang muka</b> sebesar <b>Rp. <?php echo number_format($nota['dp']) ?></b> ke BANK BNI 069984321 A/N. Futsal <br><br> <small>*</small>Sisa pembayaran dapat dibayar dilapangan setelah/sebelum main</div>
              </div>
              <div class="col-lg-6 col-md-6">
                <table class="table table-hover">
                  <tr>
                    <td><b>Nama Lapangan</b></td>
                    <td><?php echo $nota['nama_lapangan'] ?></td>
                  </tr>
                  <tr>
                    <td><b>Tanggal Booking</b></td>
                    <td><?php echo $nota['tgl_booking'] ?></td>
                  </tr>
                  <tr>
                    <td><b>Tanggal Main</b></td>
                    <td><?php echo $nota['tgl_main'] ?></td>
                  </tr>
                  <tr>
                    <td><b>Jam Main</b></td>
                    <td><?php echo $nota['jam_mulai']." - ".$nota['jam_selesai'] ?></td>
                  </tr>
                  <tr>
                    <td><b>Uang Muka</b></td>
                    <td><?php echo $nota['dp'] ?></td>
                  </tr>
                  <tr>
                    <td><b>Total Harga</b></td>
                    <td><?php echo $nota['total_harga'] ?></td>
                  </tr>
                </table>
                <button class="btn btn-primary" onclick="window.print()"><i class="fa fa-print"></i></button>
                <?php if ($nota['status_dp'] == 'Belum Dibayar'): ?>
                <a href="pembayaran.php?id_booking=<?php echo $id_booking ?>" class="btn btn-primary pull-right">Bayar Uang Muka</a>
                <?php elseif ($nota['status_dp'] == 'Sedang Diproses'): ?>
                  <span class="btn btn-info pull-right">Uang Muka Sedang Diproses</span>
                <?php else: ?>
                  <span class="btn btn-success pull-right">Uang Muka Sudah Lunas</span>
                <?php endif ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </body>
  </html>
