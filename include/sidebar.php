<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<div class="profile-sidebar">
			<div class="profile-userpic">
				<img src="../foto/admin.png" class="img-responsive" alt="">
			</div>
			<div class="profile-usertitle">
				<div class="profile-usertitle-name"><?php echo ucfirst($_SESSION['username']);?></div>
				<div class="profile-usertitle-status"><span class="indicator label-success"></span>Online</div>
			</div>
			<div class="clear"></div>
		</div>
		<div class="divider"></div>
		<ul class="nav menu">
			<li class=""><a href="../admin/index.php"><em class="fa fa-dashboard">&nbsp;</em> Dashboard</a></li>
			<li><a href="index.php?page=viewlistbooking"><em class="fa fa-book">&nbsp;</em> List Booking</a></li>
			<li><a href="index.php?page=viewpembayaran"><em class="fa fa-dollar">&nbsp;</em> Pembayaran</a></li>
			<li class="parent "><a data-toggle="collapse" href="#sub-item-1">
				<em class="fa fa-database">&nbsp;</em> Master <span data-toggle="collapse" href="#sub-item-1" class="icon pull-right"><em class="fa fa-plus"></em></span>
				</a>
				<ul class="children collapse" id="sub-item-1">
					<li><a class="" href="index.php?page=viewcustomer">
						<span class="fa fa-hotel">&nbsp;</span> Customers
					</a></li>
					<li><a class="" href="index.php?page=viewlapangan">
						<span class="fa fa-group">&nbsp;</span> Lapangan
					</a></li>
					<li><a class="" href="index.php?page=viewuser">
						<span class="fa fa-user">&nbsp;</span> User
					</a></li>
				</ul>
			</li>
			<li class="parent "><a data-toggle="collapse" href="#sub-item-3">
				<em class="fa fa-navicon">&nbsp;</em> Laporan <span data-toggle="collapse" href="#sub-item-3" class="icon pull-right"><em class="fa fa-plus"></em></span>
				</a>
				<ul class="children collapse" id="sub-item-3">
					<li><a class="" href="../laporan/laporancustomer2.php">
						<span class="fa fa-pie-chart">&nbsp;</span> Laporan Customer
					</a></li>
					<li><a class="" href="index.php?page=viewlaporanpemesanan">
						<span class="fa fa-line-chart">&nbsp;</span> Lap.Pemesanan
					</a></li>
					<li><a class="" href="index.php?page=viewlaporanpembayaran">
						<span class="fa fa-line-chart">&nbsp;</span> Lap.Pembayaran
					</a></li>
				</ul>
			</li>
			<!-- <li class="parent "><a data-toggle="collapse" href="#sub-item-4">
				<em class="fa fa-navicon">&nbsp;</em> Pengaturan <span data-toggle="collapse" href="#sub-item-4" class="icon pull-right"><em class="fa fa-plus"></em></span>
				</a>
				<ul class="children collapse" id="sub-item-4">
					<li><a class="" href="#">
						<span class="fa fa-photo">&nbsp;</span> Change Wallpaper
					</a></li>
					<li><a class="" href="#">
						<span class="fa fa-key">&nbsp;</span> Change Password
					</a></li>
				</ul>
			</li> -->
			<li><a href="logout.php"><em class="fa fa-power-off">&nbsp;</em> Logout</a></li>
		</ul>
		
	</div><!--/.sidebar-->