<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse"><span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span></button>
				<a class="navbar-brand" href="#"><span>Dashboard</a>
				<ul class="nav navbar-top-links navbar-right">
					<li class="dropdown"><a class="dropdown-toggle count-info" href="login.php">
						<em class="fa fa-power-off"></em>
					</a>
				</li>
				<?php
				$date = DATE('Y-m-d');
				$sql = $con->query("SELECT * FROM list_booking JOIN customer ON customer.id_customer=list_booking.id_customer WHERE list_booking.tgl_booking = '$date' ORDER BY tgl_booking DESC");
				$jumlah_booking = $sql->num_rows;
				while ($user = $sql->fetch_assoc()){
					?>
					<li class="dropdown"><a class="dropdown-toggle count-info" data-toggle="dropdown" href="index.php?page=viewlistbooking">
						<em class="fa fa-bell"></em><span class="label label-info"><?php echo $jumlah_booking ?></span>
					</a>
					<ul class="dropdown-menu dropdown-alerts">
						<li><a href="index.php?page=viewlistbooking">
							<div><em class="fa fa-envelope"> <?php echo $user['username']; ?> Memesan Lapangan </em>
							<?php }?>
						</a></li>
					</div>
					
				</ul>
				
			</li>
		</ul>
		
	</div>
</div><!-- /.container-fluid -->
</nav>