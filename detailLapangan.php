<?php 
session_start();
include 'include/config.php';
$id_lapangan = $_GET['id'];
// echo $id_lapangan;die;
// echo "SELECT * FROM lapangan WHERE id_lapangan='$id_lapangan'";die;
$qwr = $con->query("SELECT * FROM lapangan WHERE id_lapangan='$id_lapangan'");
$lap = mysqli_fetch_assoc($qwr);
// print_r($lap);die;
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Theme Made By www.w3schools.com - No Copyright -->
  <title>BINTANG FUTSAL</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link href="css/font-awesome.min.css" rel="stylesheet">
  <link href="css/font.css" rel="stylesheet" type="text/css">
  <script src="js/jquery-1.11.1.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <style>
    body {
      font: 400 15px/1.8 Lato, sans-serif;
      color: #777;
    }
    h3, h4 {
      margin: 10px 0 30px 0;
      letter-spacing: 10px;      
      font-size: 20px;
      color: #111;
    }
    .container {
      padding: 80px 120px;
    }
    .person {
      border: 10px solid transparent;
      margin-bottom: 25px;
      width: 80%;
      height: 80%;
      opacity: 0.7;
    }
    .person:hover {
      border-color: #f1f1f1;
    }
    .carousel-inner img {
      -webkit-filter: grayscale(90%);
      filter: grayscale(90%); /* make all photos black and white */ 
      width: 100%; /* Set width to 100% */
      margin: auto;
    }
    .carousel-caption h3 {
      color: #fff !important;
    }
    @media (max-width: 600px) {
      .carousel-caption {
        display: none; /* Hide the carousel text when the screen is less than 600 pixels wide */
      }
    }
    .bg-1 {
      background: #2d2d30;
      color: #bdbdbd;
    }
    .bg-1 h3 {color: #fff;}
    .bg-1 p {font-style: italic;}
    .list-group-item:first-child {
      border-top-right-radius: 0;
      border-top-left-radius: 0;
    }
    .list-group-item:last-child {
      border-bottom-right-radius: 0;
      border-bottom-left-radius: 0;
    }
    .thumbnail {
      padding: 0 0 15px 0;
      border: none;
      border-radius: 0;
    }
    .thumbnail p {
      margin-top: 15px;
      color: #555;
    }
    .btn {
      padding: 10px 20px;
      background-color: #333;
      color: #f1f1f1;
      border-radius: 0;
      transition: .2s;
    }
    .btn:hover, .btn:focus {
      border: 1px solid #333;
      background-color: #fff;
      color: #000;
    }
    .modal-header, h4, .close {
      background-color: #333;
      color: #fff !important;
      text-align: center;
      font-size: 30px;
    }
    .modal-body {
      padding: 40px 50px;
    }
    .nav-tabs li a {
      color: #777;
    }
    #googleMap {
      width: 100%;
      height: 400px;
      -webkit-filter: grayscale(100%);
      filter: grayscale(100%);
    }  
    .navbar {
      font-family: Montserrat, sans-serif;
      margin-bottom: 0;
      background-color: #2d2d30;
      border: 0;
      font-size: 11px !important;
      letter-spacing: 4px;
      opacity: 0.9;
    }
    .navbar li a, .navbar .navbar-brand { 
      color: #d5d5d5 !important;
    }
    .navbar-nav li a:hover {
      color: #fff !important;
    }
    .navbar-nav li.active a {
      color: #fff !important;
      background-color: #29292c !important;
    }
    .navbar-default .navbar-toggle {
      border-color: transparent;
    }
    .open .dropdown-toggle {
      color: #fff;
      background-color: #555 !important;
    }
    .dropdown-menu li a {
      color: #000 !important;
    }
    .dropdown-menu li a:hover {
      background-color: red !important;
    }
    footer {
      background-color: #2d2d30;
      color: #f5f5f5;
      padding: 32px;
    }
    footer a {
      color: #f5f5f5;
    }
    footer a:hover {
      color: #777;
      text-decoration: none;
    }  
    .form-control {
      border-radius: 0;
    }
    textarea {
      resize: none;
    }
  </style>
</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="50">

  <nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>                        
        </button>
        <a class="navbar-brand" href="#myPage"><img src="foto/logo-lap-futsal.jpg" alt="New York" width="60" height="auto"></a>
      </div>
      <div class="collapse navbar-collapse" id="myNavbar">
        <ul class="nav navbar-nav navbar-right">
          <li><a href="index.php">HOME</a></li>

          <?php if (isset($_SESSION['pelanggan'])): ?>
            <li><a href="riwayatbooking.php">RIWAYAT BOOKING</a></li>
            <li><a href="logout.php">LOGOUT</a></li>
            <!-- jika tidak ada session pelanggan -->
            <?php else: ?>
              <li><a data-toggle="modal" data-target="#login">LOGIN</a></li>
              <li><a data-toggle="modal" data-target="#register">REGISTER</a></li>
            <?php endif ?>
          </ul>
        </div>
      </div>
    </nav>

    <!-- Container (The Band Section) -->
    <div id="band" class="container text-center">
      <h3 style="margin-bottom: 0">Detail Lapangan</h3>
      <h3><?php echo $lap['nama_lapangan'] ?></h3>
      <br>
      <div class="row">
        <div class="col-sm-6">
          <a href="foto/<?php echo $lap['picture'] ?>" target="_blank"><img src="foto/<?php echo $lap['picture'] ?>" class="person" alt="Random Name" style="width: 250px;"></a>
        </div>
        <div class="col-sm-6">
          <p class="text-center"><strong>Harga : Rp. <?php echo number_format($lap['harga']) ?> / Jam</strong></p>
          <p class="text-center"><strong>Keterangan : </strong><?php echo $lap['keterangan'] ?></p>
          <p class="text-center"><strong>Fasilitas Lapangan</strong></p>
          <ol>
            <?php 
            $fasilitas = explode(',', $lap['fasilitas']);
            foreach ($fasilitas as $fasilitas) { ?>
              <li><?php echo $fasilitas ?></li>
            <?php } ?>
          </ol>
        </div>
      </div>
    </div>
    <!-- Footer -->
    <footer class="text-center">
      <a class="up-arrow" href="#myPage" data-toggle="tooltip" title="TO TOP">
        <span class="glyphicon glyphicon-chevron-up"></span>
      </a><br><br>
      <!-- <p>BINTANG FUTSAL</p>  -->
    </footer>


    <!-- Modal LOGIN -->
    <div class="modal fade" id="login" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">×</button>
            <h4>LOGIN</h4>
          </div>
          <div class="modal-body">
            <form action="logincustomer.php" role="form" method="post">
              <div class="form-group">
                <label style="color: black;"><span class="glyphicon glyphicon-tags"></span> Email</label>
                <input type="email" class="form-control" name="email" placeholder="Email">
              </div>
              <div class="form-group">
                <label style="color: black;"><span class="glyphicon glyphicon-lock"></span> Password</label>
                <input type="password" class="form-control" name="password" placeholder="Password">
              </div>
              <button type="submit" class="btn btn-block">Login 
                <span class="glyphicon glyphicon-ok"></span>
              </button>
            </form>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal REGISTER -->
    <div class="modal fade" id="register" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">×</button>
            <h4>Registrasi</h4>
          </div>
          <div class="modal-body">
            <form action="registercustomer.php" method="POST" role="form">
              <div class="form-group">
                <label style="color: black;"><span class="glyphicon glyphicon-user"></span> Nama Lengkap</label>
                <input type="text" class="form-control" name="nama_lengkap" placeholder="Nama Lengkap">
              </div>
              <div class="form-group">
                <label style="color: black;"><span class="glyphicon glyphicon-tags"></span> Username</label>
                <input type="text" class="form-control" name="username" placeholder="Username">
              </div>
              <div class="form-group">
                <label style="color: black;"><span class="glyphicon glyphicon-book"></span> Email</label>
                <input type="email" class="form-control" name="email" placeholder="Email">
              </div>
              <div class="form-group">
                <label style="color: black;"><span class="glyphicon glyphicon-lock"></span> Password</label>
                <input type="password" class="form-control" name="password" placeholder="Password">
              </div>
              <div class="form-group">
                <label style="color: black;"><span class="glyphicon glyphicon-phone"></span> No. Telepon</label>
                <input type="number" class="form-control" name="telepon" placeholder="No. Telepon">
              </div>
              <div class="form-group">
                <label style="color: black;"><span class="glyphicon glyphicon-home"></span> Alamat</label>
                <textarea class="form-control"  name="alamat" placeholder="Alamat Lengkap" rows="5"></textarea>
              </div>
              <button type="submit" class="btn btn-block">Daftar 
                <span class="glyphicon glyphicon-ok"></span>
              </button>
            </form>
          </div>
        </div>
      </div>
    </div>

    <script>
      $(document).ready(function(){
  // Initialize Tooltip
  $('[data-toggle="tooltip"]').tooltip(); 
  
  // Add smooth scrolling to all links in navbar + footer link
  $(".navbar a, footer a[href='#myPage']").on('click', function(event) {

    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {

      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 900, function(){

        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });
})
</script>

</body>
</html>
