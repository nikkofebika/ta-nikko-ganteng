<?php 
session_start();
include 'include/config.php';

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Theme Made By www.w3schools.com - No Copyright -->
	<title>BINTANG FUTSAL</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="css/font.css" rel="stylesheet" type="text/css">
	<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<style>
		body {
			font: 400 15px/1.8 Lato, sans-serif;
			color: #777;
		}
		h3, h4 {
			margin: 10px 0 30px 0;
			letter-spacing: 10px;      
			font-size: 20px;
			color: #111;
		}
		.container {
			padding: 80px 120px;
		}
		.person {
			border: 10px solid transparent;
			margin-bottom: 25px;
			width: 80%;
			height: 80%;
			opacity: 0.7;
		}
		.person:hover {
			border-color: #f1f1f1;
		}
		.carousel-inner img {
			-webkit-filter: grayscale(90%);
			filter: grayscale(90%); /* make all photos black and white */ 
			width: 100%; /* Set width to 100% */
			margin: auto;
		}
		.carousel-caption h3 {
			color: #fff !important;
		}
		@media (max-width: 600px) {
			.carousel-caption {
				display: none; /* Hide the carousel text when the screen is less than 600 pixels wide */
			}
		}
		.bg-1 {
			background: #2d2d30;
			color: #bdbdbd;
		}
		.bg-1 h3 {color: #fff;}
		.bg-1 p {font-style: italic;}
		.list-group-item:first-child {
			border-top-right-radius: 0;
			border-top-left-radius: 0;
		}
		.list-group-item:last-child {
			border-bottom-right-radius: 0;
			border-bottom-left-radius: 0;
		}
		.thumbnail {
			padding: 0 0 15px 0;
			border: none;
			border-radius: 0;
		}
		.thumbnail p {
			margin-top: 15px;
			color: #555;
		}
		.btn {
			padding: 10px 20px;
			background-color: #333;
			color: #f1f1f1;
			border-radius: 0;
			transition: .2s;
		}
		.btn:hover, .btn:focus {
			border: 1px solid #333;
			background-color: #fff;
			color: #000;
		}
		.modal-header, h4, .close {
			background-color: #333;
			color: #fff !important;
			text-align: center;
			font-size: 30px;
		}
		.modal-body {
			padding: 40px 50px;
		}
		.nav-tabs li a {
			color: #777;
		}
		#googleMap {
			width: 100%;
			height: 400px;
			-webkit-filter: grayscale(100%);
			filter: grayscale(100%);
		}  
		.navbar {
			font-family: Montserrat, sans-serif;
			margin-bottom: 0;
			background-color: #2d2d30;
			border: 0;
			font-size: 11px !important;
			letter-spacing: 4px;
			opacity: 0.9;
		}
		.navbar li a, .navbar .navbar-brand { 
			color: #d5d5d5 !important;
		}
		.navbar-nav li a:hover {
			color: #fff !important;
		}
		.navbar-nav li.active a {
			color: #fff !important;
			background-color: #29292c !important;
		}
		.navbar-default .navbar-toggle {
			border-color: transparent;
		}
		.open .dropdown-toggle {
			color: #fff;
			background-color: #555 !important;
		}
		.dropdown-menu li a {
			color: #000 !important;
		}
		.dropdown-menu li a:hover {
			background-color: red !important;
		}
		footer {
			background-color: #2d2d30;
			color: #f5f5f5;
			padding: 32px;
		}
		footer a {
			color: #f5f5f5;
		}
		footer a:hover {
			color: #777;
			text-decoration: none;
		}  
		.form-control {
			border-radius: 0;
		}
		textarea {
			resize: none;
		}
	</style>
</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="50">

	<?php include 'navbar.php'; ?>

	<div id="myCarousel" class="carousel slide" data-ride="carousel">
		<!-- Indicators -->
		<ol class="carousel-indicators">
			<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
			<li data-target="#myCarousel" data-slide-to="1"></li>
			<li data-target="#myCarousel" data-slide-to="2"></li>
		</ol>

		<!-- Wrapper for slides -->
		<div class="carousel-inner" role="listbox">
			<div class="item active">
				<img src="foto/lap-futsal1.jpg" alt="New York" width="1200" height="700">
				<div class="carousel-caption">
					<!-- <h3>Bintang Futsal</h3> -->
					<!-- <p>Bintang Futsal Adalah Salah Satu Tempat Penyewaan Lapangan Futsal yang ada di Citra Raya.</p> -->
				</div>      
			</div>

			<div class="item">
				<img src="foto/lap-futsal2.jpg" alt="Chicago" width="1200" height="700">
				<div class="carousel-caption">
					<!-- <h3>Bintang Futsal</h3> -->
					<!-- <p>Thanks Your For Visiting</p> -->
				</div>      
			</div>

			<div class="item">
				<img src="foto/lap-futsal3.jpg" alt="Los Angeles" width="1200" height="700">
				<div class="carousel-caption">
					<!-- <h3>Bintang Futsal</h3> -->
					<!-- <p>Even though the traffic was a mess, we had the best time playing at Venice Beach!</p> -->
				</div>      
			</div>
		</div>

		<!-- Left and right controls -->
		<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
		</a>
		<a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		</a>
	</div>

	<!-- Container (The Band Section) -->
  <!-- <div id="band" class="container text-center">
    <h3>PROFIL</h3>
    <p>Merupakan salah satu Lapangan yang paling Favorit, memiliki fasilitas 4 lapangan futsal indoor, yang terdiri dari 2 lapangan besar dan 2 lapangan kecil.</p>
    <br>
    <div class="row">
      <div class="col-sm-4">
        <p class="text-center"><strong>Name</strong></p><br>
        <a href="#demo" data-toggle="collapse">
          <img src="foto/customer-service-icon.png" class="img-circle person" alt="Random Name" width="255" height="255">
        </a>
        <div id="demo" class="collapse">
          <p></p>
          <p></p>
          <p></p>
        </div>
      </div>
      <div class="col-sm-4">
        <p class="text-center"><strong>Name</strong></p><br>
        <a href="#demo2" data-toggle="collapse">
          <img src="foto/owner.png" class="img-circle person" alt="Random Name" width="255" height="255">
        </a>
        <div id="demo2" class="collapse">
          <p>Pemilik Lapangan</p>
          <p>Bintang Futsal</p>
          <p>Birthday 1988</p>
        </div>
      </div>
      <div class="col-sm-4">
        <p class="text-center"><strong>ISKANDAR</strong></p><br>
        <a href="#demo3" data-toggle="collapse">
          <img src="foto/iskandar.jpeg" class="img-circle person" alt="Random Name" width="255" height="255">
        </a>
        <div id="demo3" class="collapse">
          <p>Pembuat Website</p>
          <p>Loves math</p>
          <p>Birthday 1996</p>
        </div>
      </div>
    </div>
 </div> -->

 <!-- Container (TOUR Section) -->
 <div id="tour" class="bg-1">
 	<div class="container">
 		<h4 class="text-center">BOOKING DISINI</h4>
 		<p class="text-center">Silahkan pilih lapangan yang ingin anda booking</p><br>


 		<div class="row text-center">
 			<div class="col-md-12" style="margin-left: 132px;">
 				<?php 
 				$ambil = $con->query("SELECT * FROM lapangan");
 				while($perlapangan = $ambil->fetch_assoc()){;
 					?><center>
 						<div class="col-md-4">
 							<div class="thumbnail">
 								<img src="foto/<?php echo $perlapangan['picture'] ?>" alt="Paris" width="400" height="300">
 								<p><strong><?php echo $perlapangan['nama_lapangan'] ?></strong></p>
 								<p><strong>Rp. <?php echo $perlapangan['harga'] ?>/ jam</strong></p>
 								<a href="detailLapangan.php?id=<?php echo $perlapangan['id_lapangan'] ?>" class="btn">DETAIL</a>
 								<!-- <button data-toggle="modal" data-id_lapangan="<?php //echo $perlapangan['id_lapangan'] ?>" data-target="#booking" class="btn btn-success pull-right">BOOKING</button> -->
 								<a href="formBooking.php?id=<?php echo $perlapangan['id_lapangan'] ?>" class="btn">BOOKING</a>
 							</div>
 						</div>
 					</center>
 				<?php } ?>
 			</div>
 		</div>
 	</div>

 	<!-- Modal LOGIN -->
 	<div class="modal fade" id="login" role="dialog">
 		<div class="modal-dialog">

 			<!-- Modal content-->
 			<div class="modal-content">
 				<div class="modal-header">
 					<button type="button" class="close" data-dismiss="modal">×</button>
 					<h4>LOGIN</h4>
 				</div>
 				<div class="modal-body">
 					<form action="logincustomer.php" role="form" method="post">
 						<div class="form-group">
 							<label style="color: black;"><span class="glyphicon glyphicon-tags"></span> Email</label>
 							<input type="email" class="form-control" name="email" placeholder="Email">
 						</div>
 						<div class="form-group">
 							<label style="color: black;"><span class="glyphicon glyphicon-lock"></span> Password</label>
 							<input type="password" class="form-control" name="password" placeholder="Password">
 						</div>
 						<button type="submit" class="btn btn-block">Login 
 							<span class="glyphicon glyphicon-ok"></span>
 						</button>
 					</form>
 				</div>
 			</div>
 		</div>
 	</div>

 	<!-- Modal REGISTER -->
 	<div class="modal fade" id="register" role="dialog">
 		<div class="modal-dialog">

 			<!-- Modal content-->
 			<div class="modal-content">
 				<div class="modal-header">
 					<button type="button" class="close" data-dismiss="modal">×</button>
 					<h4>Registrasi</h4>
 				</div>
 				<div class="modal-body">
 					<form action="registercustomer.php" method="POST" role="form">
 						<div class="form-group">
 							<label style="color: black;"><span class="glyphicon glyphicon-user"></span> Nama Lengkap</label>
 							<input type="text" class="form-control" name="nama_lengkap" placeholder="Nama Lengkap">
 						</div>
 						<div class="form-group">
 							<label style="color: black;"><span class="glyphicon glyphicon-tags"></span> Username</label>
 							<input type="text" class="form-control" name="username" placeholder="Username">
 						</div>
 						<div class="form-group">
 							<label style="color: black;"><span class="glyphicon glyphicon-book"></span> Email</label>
 							<input type="email" class="form-control" name="email" placeholder="Email">
 						</div>
 						<div class="form-group">
 							<label style="color: black;"><span class="glyphicon glyphicon-lock"></span> Password</label>
 							<input type="password" class="form-control" name="password" placeholder="Password">
 						</div>
 						<div class="form-group">
 							<label style="color: black;"><span class="glyphicon glyphicon-phone"></span> No. Telepon</label>
 							<input type="number" class="form-control" name="telepon" placeholder="No. Telepon">
 						</div>
 						<div class="form-group">
 							<label style="color: black;"><span class="glyphicon glyphicon-home"></span> Alamat</label>
 							<textarea class="form-control"  name="alamat" placeholder="Alamat Lengkap" rows="5"></textarea>
 						</div>
 						<button type="submit" class="btn btn-block">Daftar 
 							<span class="glyphicon glyphicon-ok"></span>
 						</button>
 					</form>
 				</div>
 			</div>
 		</div>
 	</div>
 </div>


 <!-- Image of location/map -->
 <!-- <img src="map.jpg" class="img-responsive" style="width:100%"> -->

 <!-- Footer -->
 <footer class="text-center">
 	<a class="up-arrow" href="#myPage" data-toggle="tooltip" title="TO TOP">
 		<span class="glyphicon glyphicon-chevron-up"></span>
 	</a><br><br>
 	<!-- <p>BINTANG FUTSAL</p>  -->
 </footer>





 <!-- Modal Booking -->
 <div class="modal fade" id="booking" role="dialog">
 	<div class="modal-dialog">


 		<!-- Modal content-->
 		<div class="modal-content">
 			<div class="modal-header">
 				<button type="button" class="close" data-dismiss="modal">×</button>
 				<h4>Booking <div id="#view-id_lapangan"></div></h4>
 			</div>
 			<div class="modal-body">
 				<form role="form" method="post" action="saveBooking.php">
 					<input type="hidden" name="id_customer" value="<?php echo $_SESSION['pelanggan']['id_customer'] ?> ">
 					<input type="hidden" name="id_lapangan" value="<?php echo $id_lapangan ?> ">
 					<div class="form-group">
 						<input type="hidden" readonly class="form-control" name="id_customer" value="<?php echo $_SESSION['pelanggan']['id_customer'] ?>">
 					</div>
 					<div class="form-group">
 						<input type="hidden" readonly class="form-control" name="id_lapangan" value="<?php echo $idlap ?>">
 					</div>
 					<div class="form-group">
 						<label>Jam Mulai</label>
 						<select name="jam_mulai" class="form-control" required> 
 							<option value="">-- Pilih Jam --</option>
 							<?php $ambil=$con->query("SELECT * FROM jam");
 							while($jam=$ambil->fetch_assoc()) { ?>
 								<option value="<?php echo $jam['jam']?>"><?php echo $jam['jam']?></option>
 							<?php } ?>
 						</select>
 					</div>
 					<div class="form-group">
 						<label>Jam Selesai</label>
 						<select name="jam_selesai" class="form-control" required> 
 							<option value="">-- Pilih Jam --</option>
 							<?php $ambil=$con->query("SELECT * FROM jam");
 							while($jam=$ambil->fetch_assoc()) { ?>
 								<option value="<?php echo $jam['jam']?>"><?php echo $jam['jam']?></option>
 							<?php } ?>
 						</select>
 					</div>
 					<div class="form-group">
 						<label>Tanggal Main</label>
 						<input type="date" name="tgl_main" required class="form-control">
 					</div>
 					<center>
 						<button name="booking" class="btn btn-primary">Booking</button>
 						<a onclick="history.go(-1);return false;" class="btn btn-danger">Cancel</a>
 					</center>
 				</form>



 			</div>
 		</div>
 	</div>
 </div>

 <script>
 	$(document).ready(function(){
 		$("#booking").on("show.bs.modal", function(e){
 			var button = $(e.relatedTarget);
 			console.log(e);
 			console.log(button.data("id_lapangan"));

 			$("#view-id_lapangan").text(button.data("id_lapangan"));
		});


  // Initialize Tooltip
  $('[data-toggle="tooltip"]').tooltip(); 
  
  // Add smooth scrolling to all links in navbar + footer link
  $(".navbar a, footer a[href='#myPage']").on('click', function(event) {

    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {

      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
      	scrollTop: $(hash).offset().top
      }, 900, function(){

        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
     });
    } // End if
 });
})
</script>

</body>
</html>
