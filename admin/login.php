<?php
session_start();
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Login</title>
	<link href="../css/bootstrap.min.css" rel="stylesheet">
	<link href="../css/datepicker3.css" rel="stylesheet">
	<link href="../css/styles.css" rel="stylesheet">
	<style type="text/css">
		.kotak{	
			margin-top: 10px;
		}

		.kotak .input-group{
			margin-bottom: 20px;
		}
		body{
			background: url('../foto/lapfutsal.jpg') no-repeat top left #500;
		}
		#box-login{
			margin-top: 100px;
		}
	</style>
</head>
<body>
	<div class="row">
		<div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
			<div class="login-panel panel panel-default">
				<div class="panel-heading">Log in</div>
				<div class="panel-body">
					<form role="form" action="../controller/act_login.php" method="POST"> 					
						<fieldset>
							<?php 
							echo isset($_SESSION['error']) ? '<div class="alert alert-danger">'.$_SESSION['error'].'</div>' : $e="";
							?>

							<div class="form-group">
								<input class="form-control" placeholder="Email" name="email" type="email" required autofocus="">
							</div>
							<div class="form-group">
								<input class="form-control" placeholder="Password" name="password" type="password" required value="">
							</div>
							<div class="text-danger">

							</div>
							<?php
							session_destroy();
							?>
							<button type="submit" class="btn btn-primary">Login</button></fieldset>
						</form>
					</div>
				</div>
			</div><!-- /.col-->
		</div><!-- /.row -->	


		<script src="../js/jquery-1.11.1.min.js"></script>
		<script src="../js/bootstrap.min.js"></script>
	</body>
	</html>