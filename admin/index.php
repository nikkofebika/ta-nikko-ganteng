<?php 
session_start();
if(empty($_SESSION['username'])){
	header("location:login.php");
}else{
	if(isset($_SESSION['username'])){
		$user = trim($_SESSION['username']);
	}
	if(isset($_SESSION['level'])){
		$level = trim($_SESSION['level']);
	}
	require_once('../include/config.php');
	$base_url = ('http://'.$_SERVER['HTTP_HOST'].'/sewa-lapangan/admin/index.php');
	isset ($_GET['page']) ? $page = $_GET['page'] : $page = 'home';
	?>
	<!DOCTYPE html>
	<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Menu Utama</title>
		<link href="../css/bootstrap.min.css" rel="stylesheet">
		<script type="text/javascript" src="../js/bootstrap.min.js"></script>
		<link href="../css/font-awesome.min.css" rel="stylesheet">
		<link href="../css/datepicker3.css" rel="stylesheet">
		<link href="../css/styles.css" rel="stylesheet">
		<link href="../css/dataTables.css" rel="stylesheet">
		<script src="../js/jquery-1.11.1.min.js"></script>
		<script src="../js/bootstrap.min.js"></script>
		<link href="../css/select2.css" rel="stylesheet">
		<script src="../js/select2.js"></script>

		<!--Custom Font-->
		<link href="../css/font.css" rel="stylesheet">
	</head>
	<body>
		<?php
		include '../include/header.php';
		include '../include/sidebar.php';
		?>
		<?php
	//include 'page/home.php';
	//(isset($_SESSION['pesan'])){echo $_SESSION['pesan']; unset($_SESSION['pesan']);}
		if(file_exists('../page/'.$page.'.php')){
			include('../page/'.$page.'.php');
		}else{
			echo '<div class="well">Error : Aplikasi tidak menemukan adanya file <b>'.$page.'.php </b> pada direktori <b>app/..</b></div>'; 
		}
		?>
		
		<script src="../js/chart.min.js"></script>
		<script src="../js/chart-data.js"></script>
		<script src="../js/easypiechart.js"></script>
		<script src="../js/easypiechart-data.js"></script>
		<script src="../js/bootstrap-datepicker.js"></script>
		<script src="../js/custom.js"></script>
		<script type="text/javascript" src="../js/validation.js"></script>
		<link rel="stylesheet" href="../css/jquery-ui.css">
		<script src="../js/jquery-ui.js"></script>
		<script src="../js/jquery-dataTables.js"></script>
		<script src="../js/dataTables.js"></script>


		<script type="text/javascript" src="../js/autocomplete.js"></script>
		<script type="text/javascript" src="../js/autoview.js"></script>
		<script type="text/javascript">
			$(function () {
				$('#datatables').DataTable({
					'paging'      : true,
					'searching'   : true,
					'ordering'    : true,
					'info'        : true,
					'autoWidth'   : false
				});
			});
			function isi_otomatis(){
				var nim = $("#nim").val();
				$.ajax({
					url: 'proses-ajax.php',
					data:"nim="+nim ,
				}).success(function (data) {
					var json = data,
					obj = JSON.parse(json);
					$('#nama').val(obj.nama);
					$('#jurusan').val(obj.jurusan);
					$('#alamat').val(obj.alamat);
				});
			}
			var chart4 = document.getElementById("pie-chart").getContext("2d");
			window.myPie = new Chart(chart4).Pie(pieData, {
				responsive: true,
				segmentShowStroke: false
			});
			$(function(){
				var chart = new Highcharts.Chart({
					chart: {
						renderTo: 'contohGrafikPie',
						type: 'pie',
						options3d: {
							enabled: true,
							alpha: 45,
							beta: 0
						}
					},
					title: {
						text: 'Sample Penggunaan PHP Framework'
					},
					tooltip: {
						pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b><br>Penggunaan : <b>{point.y}</b>'
					},
					plotOptions: {
						pie: {
							allowPointSelect: true,
							cursor: 'pointer',
							depth: 35,
							dataLabels: {
								enabled: true,
								format: '{point.name}'
							}
						}
					},
					series: [{
						type: 'pie',
						name: 'Penggunaan Framework',
						data: [<?= join($arrayPie,",") ?>],
					}]
				});
			});
		</script>

	</body>
	</html>
	<?php 
}
?>