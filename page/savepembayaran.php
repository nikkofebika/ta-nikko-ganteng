<?php
$carikode = $con->query("SELECT MAX(id_pembayaran) FROM pembayaran");
$datakode = mysqli_fetch_array($carikode);
	if ($datakode) {
		$nilaikode = substr($datakode[0], 2);
   		$kode = (int) $nilaikode;
   		$kode = $kode + 1;
   		$kode_otomatis = "PEMBAYARAN".str_pad($kode, 3, "0", STR_PAD_LEFT);
   	} else {
   		$kode_otomatis = "PEMBAYARAN001";
  	}
?>

<?php
	$id = $_GET['id_penyewaan'];
	$r = $con->query("SELECT * FROM penyewaan WHERE id_penyewaan = '$id' ");
	foreach($r as $rr){
		$id_customer = $rr['id_customer'];
		$id_lapangan = $rr['id_lapangan'];
		

		$s = $con->query("SELECT * FROM customer WHERE id_customer = '$id_customer' ");
		foreach($s as $ss){	
			$nama = $ss['nama_lengkap'];
			$no_telp = $ss['no_telp'];
		}

		$t = $con->query("SELECT * FROM lapangan WHERE id_lapangan = '$id_lapangan' ");
		foreach($t as $tt){	
			$nama_lapangan = $tt['nama_lapangan'];
			$size_lapangan = $tt['size_lapangan'];
			}
		}
?>

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
	<div class="row">
		<ol class="breadcrumb">
			<li><a href="#">
				<em class="fa fa-home"></em>
			</a></li>
			<li class="active">PEMBAYARAN</li>
		</ol>
	</div><!--/.row-->

	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">PEMBAYARAN</h1>
		</div>
	</div><!--/.row-->

	<div class="panel panel-default">
	<div class="panel-heading">Input Here</div>
	<div class="panel-body">
		<form role="form" action="controler/act_savepembayaran.php" method="POST" id="formpembayaran">
			<div class="form-group col-md-6">
				<label>ID PEMBAYARAN</label>
				<input class="form-control " placeholder="ID Payment" type="text" name="txtIdPayment" value="<?php echo "$kode_otomatis"; ?>" readonly required>
			</div>
			<div class="form-group col-md-6">
				<label>ID Penyewaan</label>
				<input class="form-control " placeholder="ID Reservation" type="text" name="txtIdReservation" value="<?php echo $rr['id_reservation'];?>" readonly required>
			</div>
			<div class="form-group col-md-3">
				<label>ID Customer</label>
				<input class="form-control" placeholder="NIK Customer" type="text" name="txtNikCustomer" id="txtNikCustomer" value="<?php echo $rr['nik_customer'];?>" required readonly>
			</div>
			<div class="form-group col-md-9">
				<label>Nama Customer</label>
				<input class="form-control" placeholder="Name Customer" type="text" name="txtNameCustomer" id="txtNameCustomer" value="<?php echo $name_customer;?>" required readonly>
			</div>
			<div class="form-group col-md-2">
				<label>ID Lapangan</label>
				<input class="form-control" placeholder="Id Lapangan" type="text" name="txtIdRoom" id="txtIdRoom" value="<?php echo $rr['id_room'];?>" required readonly>
			</div>
			<div class="form-group col-md-2">
				<label>Nama Lapangan</label>
				<input class="form-control" placeholder="Nama Lapangan" type="text" name="txtType" id="txtType" value="<?php echo $name_type;?>" required readonly>
			</div>
			<div class="form-group col-md-2">
				<label>Size Lapangan</label>
				<input class="form-control" placeholder="Grandtotal" type="text" name="txtGrandtotal" id="txtGrandtotal" value="<?php echo $rr['grandtotal'];?>" readonly>
			</div>
			<div class="form-group col-md-2">
				<label>Tanggal Pesan</label>
				<input class="form-control" placeholder="DP" type="text" name="txtDP" value="<?php echo $rr['down_payment'];?>"  required readonly>
			</div>
			<div class="form-group col-md-2">
				<label>Jam Mulai</label>
				<input class="form-control" placeholder="DP" type="text" name="txtDP" value="<?php echo $rr['down_payment'];?>"  required readonly>
			</div>
			<div class="form-group col-md-2">
				<label>Jam Selesai</label>
				<input class="form-control" placeholder="Grandtotal" type="text" name="txtGrandtotal" id="txtGrandtotal" value="<?php echo $rr['grandtotal'];?>" readonly>
			</div>
			<div class="form-group col-md-2">
				<label>Keterangan</label>
				<input class="form-control" placeholder="Grandtotal" type="text" name="txtGrandtotal" id="txtGrandtotal" value="<?php echo $rr['grandtotal'];?>" readonly>
			</div>
			<div class="form-group col-md-2">
				<label>Total Pembayaran</label>
				<input class="form-control txtCash" placeholder="Cash" type="text" name="txtCash" id="txtCash" min="0" onkeypress="return OnlyNumber(event)" required autofocus>
			</div>
			<div class="form-group col-md-2">
				<label>Change Money</label>
				<input class="form-control" placeholder="Change Money" type="text" name="txtChange" id="txtChange" required readonly>
			</div>
			<div class="col-md-12">
				<button type="submit" class="btn btn-primary">Pay</button>
				<a onclick="history.go(-1);return false;" class="btn btn-danger">Back</a>
			</div>
		</form>
	</div>
</div>
<?php
}
?>