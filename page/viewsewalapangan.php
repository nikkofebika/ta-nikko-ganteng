<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="index.php">
					<em class="fa fa-home"></em>
				</a></li>
                <li class="active">Reservations</li>
			</ol>
		</div><!--/.row-->
        <div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Reservations</h1>
			</div>
		</div><!--/.row-->
        
    <div class="panel panel-default">
                <div class="panel-heading"><a href="index.php?page=savereservations" class="btn btn-primary">Add</a></div>
                <div class="panel-body">
                    <div class="col-md-12">
					<div class="table-responsive">
					
					<table class="table table-striped table-condensen">
					<tr>
				     <th>ID Penyewaan</th>
					 <th>ID Lapangan</th>
					 <th>Tanggal Pesan</th>
					 <th>Tanggal Sewa</th>
					 <th>Jam Mulai</th>
					 <th>Jam Selesai</th>
					 <th>Total Harga</th>
					</tr>
					<?php
					$r = $con->query("SELECT penyewaan.id_penyewaan as id, penyewaan.id_lapangan as checkin, penyewaan.date_checkout as checkout, customers.name_customer as customer, reservations.id_room as idroom FROM reservations INNER JOIN customers ON reservations.nik_customer = customers.nik_customer WHERE reservations.status='0'");
					while ($rr = $r->fetch_array()){
						?>
						<tr>
						<td><?php echo $rr['id'];?></td>
						<td><?php echo $rr['checkin'];?></td>
						<td><?php echo $rr['checkout'];?></td>
						<td><?php echo $rr['customer'];?></td>
						<td><?php echo $rr['idroom'];?></td>
						</tr>
						<?php
					}
				?>	
					</table>
                    </div>
					</div>
                </div>
            </div><!-- /.panel-->
</div>