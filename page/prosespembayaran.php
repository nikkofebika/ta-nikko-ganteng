<?php 
	$id_booking = $_GET['id_booking'];

	$ambil = $con->query("SELECT * FROM list_booking WHERE id_booking='$id_booking'");
	$bayar = $ambil->fetch_assoc();
	$sisabayar = $bayar['total_harga'] - $bayar['dp'];
 ?>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
	<div class="row">
		<ol class="breadcrumb">
			<li><a href="index.php?page=home">
				<em class="fa fa-home"></em>
			</a></li>
			<li class="active">Pembayaran</li>
		</ol>
	</div><!--/.row-->

	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Pembayaran</h1>
		</div>
	</div><!--/.row-->

	<div class="panel panel-default">
       <div class="panel-body">
          <div class="col-md-12">
          	<div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title text-center"><b>Form Pembayaran</b></h3><br>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" method="post">
              <div class="box-body">
                <div class="form-group">
                  <label class="col-sm-2 control-label">Operator</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" value="<?php echo $_SESSION['username'] ?>" disabled>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Uang Muka</label>
                  <div class="col-sm-10">
                    <input type="number" class="form-control" disabled placeholder="Rp. <?php echo number_format($bayar['dp']) ?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Total Harga</label>
                  <div class="col-sm-10">
                    <input type="number" class="form-control" disabled placeholder="Rp. <?php echo number_format($bayar['total_harga']) ?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Sisa yang harus dibayar</label>
                  <div class="col-sm-10">
                    <input type="number" class="form-control" disabled placeholder="Rp. <?php echo number_format($sisabayar) ?>">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Input Uang Bayar</label>
                  <div class="col-sm-10">
                    <input type="number" name="bayar" class="form-control" placeholder="Rp. ">
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
              	<center>
	                <button name="simpan" class="btn btn-info">Simpan</button>
	                <a onclick="history.go(-1);return false;" class="btn btn-danger">Cancel</a>
	            </center>
              </div>
              <!-- /.box-footer -->
            </form>
            <?php 
            	if (isset($_POST['simpan'])) {

                // echo $id_booking."<br>";
                // echo "<pre>";
                // print_r($_POST);
                // echo "</pre>"."<br>";die;
            		
	            	$tgl_pembayaran = date("Y-m-d");
	            	$uang_muka = $bayar['dp'];
	            	$total_pembayaran = $bayar['total_harga'];
	            	$uang_bayar = $_POST['bayar'];

				if($uang_bayar < $sisabayar){
				echo "<div class='alert alert-info'>Uang Anda Kurang</div>";
				}else{
				// $con->query("INSERT INTO pembayaran (id_booking, operator, tgl_pembayaran, uang_muka, total_pembayaran) VALUES ('$id_booking','$operator','$tgl_pembayaran','$uang_muka','$total_pembayaran') ");
          $con->query("UPDATE pembayaran SET tgl_pembayaran='$tgl_pembayaran', status='1' WHERE id_booking='$id_booking'");

	            	$con->query("UPDATE list_booking SET status='Sudah Main' WHERE id_booking='$id_booking'");

	            	$kembalian = $uang_bayar - $sisabayar;
	            	echo "<div class='alert alert-info'>Uang kembali : Rp. ". number_format($kembalian) ."</div>";
                    echo "<meta http-equiv='refresh' content='3; url=../admin/index.php?page=viewpembayaran'>";

				}

	            	// echo "<script>alert('Proses berhasil. Data telah disimpan');window.location='../admin/index.php?page=viewpembayaran'</script>";
	            }
             ?>
          </div>
        </div>
    </div>
</div>
</div>