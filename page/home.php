	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#">
					<em class="fa fa-home"></em>
				</a></li>
				<li class="active">Dashboard</li>
			</ol>
		</div><!--/.row-->
		
		<center><marquee direction="up" scrollamount="2" style="color: black; font-family: Verdana,Arial,Helvetica,Georgia; font-size: 30px;" align="center" behavior="alternate" width="100%"><marquee direction="right" behavior="alternate"> Selamat Datang, <?php echo ucfirst($_SESSION['username']);?> </marquee></marquee></center>
		<div class="panel panel-container">
				<div class="row">
				<?php
				$sql = $con->query("SELECT * FROM customer");
				$result = $sql->num_rows;
			?>
			<a href="index.php?page=viewcustomer">
			<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
				<div class="panel panel-red panel-widget">
					<div class="panel-body easypiechart-panel">
						<h4>Customer</h4>
						<div class="easypiechart" id="easypiechart-blue" data-percent="<?php echo $result;?>" ><span class="percent"><?php echo $result;?></span></div>
					</div>
				</div>
			</div></a>
			<?php
				$sql = $con->query("SELECT * FROM list_booking WHERE status_dp = 'Lunas' OR status_dp = 'Sedang Diproses'");
				$result = $sql->num_rows;
			?>
			<a href="index.php?page=viewlistbooking">
			<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
				<div class="panel panel-red panel-widget">
					<div class="panel-body easypiechart-panel">
						<h4>Booking</h4>
						<div class="easypiechart" id="easypiechart-orange" data-percent="<?php echo $result;?>" ><span class="percent"><?php echo $result;?></span></div>
					</div>
				</div>
			</div></a>
			<?php
				$sql = $con->query("SELECT * FROM list_booking WHERE status = 'Belum Main'");
				$result = $sql->num_rows;
			?>
			<a href="index.php?page=viewpembayaran">
			<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
				<div class="panel panel-red panel-widget">
					<div class="panel-body easypiechart-panel">
						<h4>Pembayaran</h4>
						<div class="easypiechart" id="easypiechart-teal" data-percent="<?php echo $result;?>" ><span class="percent"><?php echo $result;?></span></div>
					</div>
				</div>
			</div></a>
			<?php
				$date = DATE('m');
				$sql = $con->query("SELECT * FROM list_booking WHERE month(list_booking.tgl_booking) = '$date'");
				$result = $sql->num_rows;
			?>
				<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
					<div class="panel panel-red panel-widget ">
						<div class="row no-padding"><em class="fa fa-xl fa-calendar color-red"></em>
							<div class="large"><?php echo $result; ?></div>
							<div class="text-muted">List Booking Bulan ini</div>
						</div>
					</div>
				</div>
			<div class="row">
				<?php
				$sql = "SELECT * FROM list_booking ORDER BY tgl_booking DESC";
				$result = $con->query($sql);
				$arrayPie = array();
				if ($result->num_rows > 0) {
					while($row = $result->fetch_assoc()) {
						$arrayPie[] =  "["."'".$row['tgl_booking']."'".",".$row['tgl_booking']."]";
					}
				} ?>
    <div class="col-lg-12">
        <div id="contohGrafikPie"></div>
    </div>
</div>

			
		<!--/.content-->
		<div class="page-content text-center">

                    

                </div>
		<!--/.content-->
	</div>
</div>
</div>
</div>
</div>
	<!--/.main-->
	