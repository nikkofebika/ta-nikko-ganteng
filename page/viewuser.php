<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
	<div class="row">
		<ol class="breadcrumb">
			<li><a href="index.php?page=home">
				<em class="fa fa-home"></em>
			</a></li>
			<li class="active">User</li>
		</ol>
	</div><!--/.row-->

	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Master User</h1>
		</div>
	</div><!--/.row-->

	<div class="panel panel-default">
    	<div class="panel-heading"><a href="index.php?page=saveuser" class="btn btn-primary">Tambah</a></div>
    	<div class="panel-body">
    		<div class="col-md-12">
    			<div class="table-responsive">
                    <table class="table table-striped">
                        <tr>
                            <th>No</th>
                            <th>ID Admin</th>
                            <th>Username</th>
    						<th>Email</th>
                            <th>Picture</th>
                            <th>Actions</th>
                        </tr>
                        <?php
                        $no=1;
                        $ambil = $con->query("SELECT * FROM user");
                        while ($user = $ambil->fetch_assoc()) {
                            ?>
                            <tr>
                                <td><?php echo $no++; ?></td>
                                <td><?php echo $user['id_user']; ?></td>
                                <td><?php echo $user['username']; ?></td>
                                <td><?php echo $user['email']; ?></td>
                                <td><img src="../foto/<?php echo $user['picture']; ?>" class="img-responsive img-circle" width="70"></td>
                                <td><a href="../admin/index.php?page=edituser&id_user=<?php echo $user['id_user'];?>" class="btn btn-sm btn-success">Edit</a> <a href="../controller/act_deluser.php?id_user=<?php echo $user['id_user'];?>" class="btn btn-sm btn-danger" onclick="return confirm('Yakin ingin hapus data ?')"> Hapus</a></td>
                            </tr>
                            <?php
                        }
                        ?>
                    </table>
                </div>
    		</div>
    	</div>
    </div>
</div>