<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
	<div class="row">
		<ol class="breadcrumb">
			<li><a href="#">
				<em class="fa fa-home"></em>
			</a></li>
			<li class="active">Booking</li>
		</ol>
	</div><!--/.row-->

	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Input Data Booking</h1>
		</div>
	</div><!--/.row-->

	<div class="panel panel-default">
		<div class="panel-heading">Input Data</div>
		<div class="panel-body">
			<div class="col-md-12"">
				<form role="form" action="../controller/act_savelistbooking.php" method="post">
					<div class="form-group">
						<label>Customer</label>
						<select name="id_customer" class="form-control"> 
							<option value="">-- Pilih Member --</option>
							<?php $ambil=$con->query("SELECT * FROM customer");
							while($customer=$ambil->fetch_assoc()) { ?>
								<option value="<?php echo $customer['id_customer']?>"><?php echo $customer['username']?></option>
							<?php } ?>
						</select>
					</div>
					<div class="form-group">
						<label>Lapangan</label>
						<select name="id_lapangan" class="form-control" required> 
							<option value="">-- Pilih Lapangan --</option>
							<?php $ambil=$con->query("SELECT * FROM lapangan");
							while($lapangan=$ambil->fetch_assoc()) { ?>
								<option value="<?php echo $lapangan['id_lapangan']?>"><?php echo $lapangan['nama_lapangan']?> &nbsp; &nbsp; <small>Rp. <?php echo number_format($lapangan['harga']) ?>/jam</small></option>
							<?php } ?>
						</select>
					</div>
					<div class="form-group">
						<label>Jam Mulai</label>
						<select name="jam_mulai" class="form-control" required> 
							<option value="">-- Pilih Jam --</option>
							<?php $ambil=$con->query("SELECT * FROM jam");
							while($jam=$ambil->fetch_assoc()) { ?>
								<option value="<?php echo $jam['jam']?>"><?php echo $jam['jam']?></option>
							<?php } ?>
						</select>
					</div>
					<div class="form-group">
						<label>Jam Selesai</label>
						<select name="jam_selesai" class="form-control" required> 
							<option value="">-- Pilih Jam --</option>
							<?php $ambil=$con->query("SELECT * FROM jam");
							while($jam=$ambil->fetch_assoc()) { ?>
								<option value="<?php echo $jam['jam']?>"><?php echo $jam['jam']?></option>
							<?php } ?>
						</select>
					</div>
					<div class="form-group">
						<label>Tanggal Main</label>
						<input type="date" name="tgl_main" required class="form-control">
					</div>
					<button type="submit" class="btn btn-primary">Save</button>
					<a onclick="history.go(-1);return false;" class="btn btn-danger">Back</a>
				</form>
			</div>
		</div>
	</div>