<?php
	$id = $_GET['id_customer'];
	$r = $con->query("SELECT * FROM customer WHERE id_customer = '$id'");
	foreach ($r as $rr) {
		
?>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
	<div class="row">
		<ol class="breadcrumb">
			<li><a href="index.php?page=home">
				<em class="fa fa-home"></em>
			</a></li>
            <li>
            <a href="index.php?page=viewcustomer">Customer</a>
            </li>
			<li class="active">Edit Customer</li>
		</ol>
	</div><!--/.row-->

	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Master Customer</h1>
		</div>
	</div><!--/.row-->

	<div class="panel panel-default">
		<div class="panel-heading">Input Here</div>
		<div class="panel-body">
			<div class="col-md-12">
				<form role="form" action="../controller/act_updatecustomer.php" method="POST">
					<div class="form-group">
						<label>Nama Lengkap</label>
						<input class="form-control" type="text" value="<?php echo $rr['nama_lengkap'];?>" placeholder="Nama Lengkap" name="nama_lengkap">
					</div>
					<div class="form-group">
						<label>Username</label>
						<input class="form-control" type="text" value="<?php echo $rr['username'];?>" placeholder="Username" name="username">
						<input type="hidden" value="<?php echo $rr['id_customer'];?>" name="id_customer">
					</div>
					<div class="form-group">
						<label>Email</label>
						<input class="form-control" placeholder="Email" type="text" value="<?php echo $rr['email'];?>" name="email" required>
					</div>
					<div class="form-group">
						<label>Password</label>
						<input class="form-control" placeholder="Password" type="password" value="<?php echo $rr['password'];?>" name="password" required>
					</div>
					<div class="form-group">
						<label>No Telepon</label>
						<input class="form-control" placeholder="No Telepon" type="number" value="<?php echo $rr['no_telp'];?>" name="no_telp" required>
					</div>
	                <div class="form-group">
						<label>Alamat</label>
						<textarea class="form-control" placeholder="Alamat Lengkap" name="alamat" required><?php echo $rr['alamat']; ?></textarea>
					</div>
					<button type="submit" class="btn btn-primary">Update</button>
					<a onclick="history.go(-1);return false;" class="btn btn-danger">Back</a>
				</form>
			</div>
		</div>
	</div>
</div>
<?php
	}
?>