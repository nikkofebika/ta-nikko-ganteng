<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
	<div class="row">
		<ol class="breadcrumb">
			<li><a href="index.php?page=home">
				<em class="fa fa-home"></em>
			</a></li>
			<li class="active">Pembayaran</li>
		</ol>
	</div><!--/.row-->

	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Data Pembayaran</h1>
		</div>
	</div><!--/.row-->

	<div class="panel panel-default">
       <div class="panel-body">
          <div class="col-md-12">
             <div class="table-responsive">
                <table class="table table-striped">
                    <tr>
                        <th>No</th>
                        <th>Customer</th>
                        <th>Lapangan</th>
                        <th>Lama Main</th>
                        <th>Tgl Main</th>
                        <th>Uang Muka</th>
                        <th>Total Harga</th>
                        <th>Sisa Bayar</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                    <?php
                    $no=1;
                    $query = "SELECT customer.username, lapangan.nama_lapangan, lapangan.harga, list_booking.id_booking, list_booking.jam_mulai, list_booking.jam_selesai, list_booking.tgl_booking, list_booking.tgl_main, list_booking.dp, list_booking.total_harga, list_booking.status 
                        FROM 
                        list_booking 
                        JOIN customer ON customer.id_customer=list_booking.id_customer
                        JOIN lapangan ON lapangan.id_lapangan=list_booking.id_lapangan
                        WHERE  list_booking.status_dp = 'Lunas'";
                        // WHERE list_booking.status = 'Belum Main' AND list_booking.status_dp = 'Lunas'";
                    $ambil = $con->query($query);
                    while ($booking = $ambil->fetch_assoc()) {
                        ?>
                        <tr>
                            <td><?php echo $no++; ?></td>
                            <td><?php echo $booking['username']; ?></td>
                            <td><?php echo $booking['nama_lapangan']; ?></td>
                            <td>
                                <?php 
                                    $mulai = strtotime($booking['jam_mulai']);
                                    $selesai = strtotime($booking['jam_selesai']);
                                    $totaljam = ($selesai - $mulai)/3600;
                                    echo $totaljam." Jam";
                                 ?>
                            </td>
                            <td><?php echo $booking['tgl_main']; ?></td>
                            <td>Rp. <?php echo number_format($booking['dp']); ?></td>
                            <td>Rp.<?php echo number_format($booking['total_harga']); ?></td>
                            <td><span class="label label-warning label-lg" style="font-size: 14px;">
                                <?php 
                                    $sisabayar = $booking['total_harga'] - $booking['dp'];
                                    echo "Rp. ".number_format($sisabayar);
                                ?></span>
                            </td>
                            <td><?php echo $booking['status'] == 'Sudah Main' ? '<div class="label label-success">'.$booking['status'].'</div>' : '<div class="label label-danger">'.$booking['status'].'</div>' ?></td>
                            <td>
                                <?php if ($booking['status'] == 'Belum Main'): ?>
                                    <a href="../admin/index.php?page=prosespembayaran&id_booking=<?php echo $booking['id_booking'] ?>" class="btn btn-primary">Bayar</a>
                                <?php endif ?>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                </table>
            </div>
        </div>
    </div>
</div>
</div>