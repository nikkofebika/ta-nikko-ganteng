<style type="text/css">
	.select2-container{
		width: 100% !important;
	}
	.select2-container--classic .select2-search--inline .select2-search__field{
		width: 100% !important;
	}

</style>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
	<div class="row">
		<ol class="breadcrumb">
			<li><a href="#">
				<em class="fa fa-home"></em>
			</a></li>
			<li class="active">Lapangan</li>
		</ol>
	</div><!--/.row-->

	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Input Data Lapangan</h1>
		</div>
	</div><!--/.row-->

	<div class="panel panel-default">
		<!-- <div class="panel-heading">Input Here</div> -->
		<div class="panel-body">
			<div class="col-md-12">
				<form role="form" action="../controller/act_savelapangan.php" method="post" enctype="multipart/form-data">
					<div class="form-group">
						<label>Nama Lapangan</label>
						<input class="form-control" placeholder="Nama Lapangan" name="txtnamalp" type="text" required="required">
					</div>
					<div class="form-group">
						<label>Size Lapangan</label>
						<select class="form-control" name="cmbsize">
							<option>Pilih Size</option>
							<option value="large">Large</option>
							<option value="small">Small</option>
						</select>
						<div class="form-group">
							<label>Foto Lapangan</label>
							<input class="form-control" name="fotolp" type="file" maxlength="3" required>
						</div>
						<div class="form-group">
							<label>Harga</label>
							<input class="form-control" placeholder="Harga" name="txtharga" type="number" maxlength="11" required>
						</div>
						<div class="form-group">
							<label>Fasilitas</label>
							<select class="js-example-basic-multiple" name="fasilitas[]" multiple="multiple">
							</select>
						</div>
						<div class="form-group">
							<label>Keterangan</label>
							<textarea class="form-control" placeholder="keterangan" name="txtket" type="textarea" rows="5"  maxlength="40" required></textarea>
						</div>
						<button type="submit" class="btn btn-primary">Save</button>
						<a onclick="history.go(-1);return false;" class="btn btn-danger">Back</a>			
					</form>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		$(document).ready(function() {
			$('.js-example-basic-multiple').select2({
				placeholder: "Isi fasilitas lapangan disini",
				tags: true,
				theme: "classic",
				allowClear: true
			});
		});
	</script>