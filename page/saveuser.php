<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#">
					<em class="fa fa-home"></em>
				</a></li>
				<li class="active">Admin</li>
			</ol>
		</div><!--/.row-->
		
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Input Data Admin</h1>
			</div>
		</div><!--/.row-->
		
		<div class="panel panel-default">
			<!-- <div class="panel-heading">Input Data</div> -->
			<div class="panel-body">
				<div class="col-md-12">
					<form role="form" action="../controller/act_saveuser.php" method="post" enctype="multipart/form-data">
						<div class="form-group">
							<label>Username</label>
							<input class="form-control" placeholder="username" name="username" type="text"required="required">
						</div>
						<div class="form-group">
							<label>Email</label>
							<input class="form-control" placeholder="Email" name="email" type="email" required="required">
						</div>
						<div class="form-group">
							<label>Password</label>
							<input class="form-control" placeholder="Password" name="password" type="password" maxlength="20" required>
						</div>
						<div class="form-group">
							<label>Foto User</label>
							<input class="form-control" name="fotouser" type="file">
						</div>
						<button type="submit" class="btn btn-primary">Save</button>
							<a onclick="history.go(-1);return false;" class="btn btn-danger">Back</a>
					</form>
				</div>
			</div>
</div>