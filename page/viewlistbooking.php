<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
	<div class="row">
		<ol class="breadcrumb">
			<li><a href="index.php?page=home">
				<em class="fa fa-home"></em>
			</a></li>
			<li class="active">Booking</li>
		</ol>
	</div><!--/.row-->

	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Data Booking</h1>
		</div>
	</div><!--/.row-->

	<div class="panel panel-default">
		<div class="panel-heading"><a href="index.php?page=savelistbooking" class="btn btn-primary">Tambah</a></div>
		<div class="panel-body">
			<div class="col-md-12">
				<div class="table-responsive">
					<table id="datatables" class="table table-striped table-hover">
						<thead>
							<tr>
								<th>No</th>
								<th>Customer</th>
								<th>Lapangan</th>
								<th>Harga</th>
								<th>Jam Mulai</th>
								<th>Jam Selesai</th>
								<th>Tgl Booking</th>
								<th>Tgl Main</th>
								<th>Uang Muka</th>
								<th>Total Harga</th>
								<th>Status</th>
								<th width="11%">Actions</th>
							</tr>
						</thead>
						<tbody>
							<?php
							$no=1;
							$query = "SELECT customer.username, lapangan.nama_lapangan, lapangan.harga, list_booking.id_booking, list_booking.jam_mulai, list_booking.jam_selesai, list_booking.tgl_booking, list_booking.tgl_main, list_booking.dp, list_booking.total_harga, list_booking.status_dp
							FROM 
							list_booking 
							JOIN customer ON customer.id_customer=list_booking.id_customer
							JOIN lapangan ON lapangan.id_lapangan=list_booking.id_lapangan ";
							$ambil = $con->query($query);
							while ($booking = $ambil->fetch_assoc()) { ?>
								<tr>
									<td><?php echo $no++; ?></td>
									<td><?php echo $booking['username']; ?></td>
									<td><?php echo $booking['nama_lapangan']; ?></td>
									<td><?php echo $booking['harga']; ?></td>
									<td><?php echo $booking['jam_mulai']; ?></td>
									<td><?php echo $booking['jam_selesai']; ?></td>
									<td><?php echo $booking['tgl_booking']; ?></td>
									<td><?php echo $booking['tgl_main']; ?></td>
									<td><?php echo $booking['dp']; ?></td>
									<td><?php echo $booking['total_harga']; ?></td>
									<td>
										<?php if ($booking['status_dp'] == 'Sedang Diproses'){ ?>
											<div class="badge badge-danger">Menunggu Konfirmasi</div>
										<?php } else { ?>
											<?php echo $booking['status_dp']; ?>
										<?php } ?>
									</td>
									<td>
										<a href="../controller/act_dellistbooking.php?id_booking=<?php echo $booking['id_booking'];?>" class="btn btn-danger" title="Hapus" onclick="return confirm('Yakin ingin hapus data booking <?php echo $booking['username'] ?> ?')"> <i class="fa fa-trash"></i></a>
										<?php if ($booking['status_dp'] =='Sedang Diproses'): ?>
											<a href="../admin/index.php?page=prosesdp&id_booking=<?php echo $booking['id_booking'];?>" class="btn btn-primary" title="Proses"><i class="fa fa-check"></i></a>
										<?php endif ?>
										<!-- <a href="#"
										data-id="<?php// echo $booking['kode_admin'] ?>"
										data-username="<?php// echo $booking['username'] ?>"
										data-email="<?php// echo $booking['email'] ?>"
										data-level="<?php// echo $booking['level'] ?>"
										data-foto="<?php// echo $booking['picture'] ?>"
										class="btn btn-xs btn-info" title="View <?php// echo $booking['username'] ?>" data-toggle="modal" data-target="#view_detail"><i class="fa fa-eye"></i></a> -->
									</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- MODAL VIEW Detail -->
<div id="view_user" class="modal fade" role="dialog">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-body">
				<div class="box box-primary">
					<div class="box-body box-profile">
						<center><div id="view-foto"></div></center>
						<!-- <img class="profile-user-img img-responsive img-circle" src="../assets/backend/dist/img/user4-128x128.jpg" alt="User profile picture"> -->

						<h3 class="profile-username text-center" id="view-username"></h3>

						<p class="text-muted text-center" id="view-level"></p>
						<p class="text-muted text-center" id="view-email"></p>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>

	</div>
</div>
<!-- MODAL VIEW Detail -->

<script type="text/javascript">
	$(document).ready(function(){
		$("#view_detail").on("show.bs.modal", function(e){
			var button = $(e.relatedTarget);
			console.log(e);

			$("#view-username").text(button.data("username"));
			$("#view-level").text(button.data("level"));
			$("#view-email").text(button.data("email"));
			var fotoo = button.data("foto");
			console.log(fotoo);
			$("#view-foto").html('<img class="profile-user-img img-circle" src="foto/'+ fotoo +' " alt="User profile picture" width="100">');
		});
	});
</script>