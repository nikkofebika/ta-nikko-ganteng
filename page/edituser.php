<?php
	$id = $_GET['id_user'];
	$r = $con->query("SELECT * FROM user WHERE id_user = '$id'");
	foreach ($r as $rr) {
		
?>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
	<div class="row">
		<ol class="breadcrumb">
			<li><a href="index.php?page=home">
				<em class="fa fa-home"></em>
			</a></li>
            <li>
            <a href="index.php?page=viewusers">Users</a>
            </li>
			<li class="active">Edit Users</li>
		</ol>
	</div><!--/.row-->

	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Master Users</h1>
		</div>
	</div><!--/.row-->

	<div class="panel panel-default">
		<div class="panel-heading">Input Here</div>
		<div class="panel-body">
			<div class="col-md-12">
				<form role="form" action="../controller/act_updateuser.php" method="POST" enctype="multipart/form-data">
					<div class="form-group">
						<label>Username</label>
						<input class="form-control" type="text" value="<?php echo $rr['username'];?>" placeholder="Username" name="username">
						<input type="hidden" value="<?php echo $rr['id_user'];?>" name="id_user">
					</div>
					<div class="form-group">
						<label>Email</label>
						<input class="form-control" placeholder="Email" type="email" value="<?php echo $rr['email'];?>" name="email" required>
					</div>
					<div class="form-group">
						<label>Password</label>
						<input class="form-control" placeholder="Password" type="password" value="<?php echo $rr['password'];?>" name="password" required>
					</div>
					<div class="form-group">
				 		<label>Ubah Foto</label>
				 		<img src="../foto/<?php echo $rr['picture']; ?>" class="img-responsive img-circle" width="100"><br>
				 		<input type="file" name="foto"></input>
				 	</div>
					<button type="submit" class="btn btn-primary">Update</button>
					<a onclick="history.go(-1);return false;" class="btn btn-danger">Back</a>
				</form>
			</div>
		</div>
	</div>
</div>
<?php
	}
?>