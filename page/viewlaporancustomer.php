<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#">
					<em class="fa fa-home"></em>
				</a></li>
				<li class="active">Laporan Data Customer</li>
			</ol>
		</div><!--/.row-->
		
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Laporan Data Customer</h1>
			</div>
		</div><!--/.row-->
		
		<div class="panel panel-default">
		<div class="panel-heading">
			Pilih Tipe Laporan
		</div>
			<div class="panel-body">
			<form class="form-horizontal" action="../laporan/laporancustomer.php" method="post" target="_blank">
                <div class="form-group">
                    <label for="" class="control-label col-sm-1">Periode</label>
                    <div class="col-sm-5">
                        <select name="tg1" class="form-control" required>
                            <option value="">--Bulan--</option>
                            <option value="1">Januari</option>
                            <option value="2">Februari</option>
                            <option value="3">Maret</option>
                            <option value="4">April</option>
                            <option value="5">Mei</option>
                            <option value="6">Juni</option>
                            <option value="7">Juli</option>
                            <option value="8">Agustus</option>
                            <option value="9">September</option>
                            <option value="10">Oktober</option>
                            <option value="11">November</option>
                            <option value="12">Desember</option>
                        </select>
                    </div>
                    <div class="col-sm-6">
                        <select name="th1" class="form-control" required>
                            <option value="">--Tahun--</option>
                            <?php
                            for ($i=2017; $i<=2050; $i++) {
                                ?>
                                <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="btn-group col-sm-9 col-sm-offset-1">
                        <button class="btn btn-primary" type="submit" >Tampilkan</button>
                        <button class="btn btn-primary" type="reset" >Reset</button>
                    </div>
                </div>
            </form>
			</div>
		</div>