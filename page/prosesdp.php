<?php 
$id_booking = $_GET['id_booking'];
 ?>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
	<div class="row">
		<ol class="breadcrumb">
			<li><a href="index.php?page=home">
				<em class="fa fa-home"></em>
			</a></li>
			<li class="active">Proses</li>
		</ol>
	</div><!--/.row-->

	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Bukti Pembayaran Uang Muka</h1>
		</div>
	</div><!--/.row-->

	<div class="panel panel-default">
       <div class="panel-body">
          <div class="col-md-12">
             <div class="table-responsive">
                <table class="table table-striped table-hover">
                    <tr>
                        <th>Customer</th>
                        <th>Tanggal Pembayaran</th>
                        <th>Bank</th>
                        <th>Uang Muka</th>
                        <th>Bukti Transfer</th>
                    </tr>
                    <?php
                    $no=1;
                    $query = "SELECT customer.username, pembayaran.tgl_transfer, list_booking.dp, pembayaran.bukti_transfer, bank.nama_bank as nama_bank
                        FROM 
                        list_booking 
                        JOIN customer ON customer.id_customer=list_booking.id_customer
                        JOIN pembayaran ON pembayaran.id_booking=list_booking.id_booking
                        LEFT JOIN bank ON bank.id_bank=pembayaran.id_bank
                        WHERE list_booking.id_booking='$id_booking'";
                    $ambil = $con->query($query);
                    while ($proses = $ambil->fetch_assoc()) {
                        ?>
                        <tr>
                            <td><?php echo $proses['username']; ?></td>
                            <td><?php echo $proses['tgl_transfer']; ?></td>
                            <td><?php echo $proses['nama_bank']; ?></td>
                            <td>Rp. <?php echo number_format($proses['dp']) ?></td>
                            <td><a target="_blank" href="../foto_bukti_pembayaran/<?php echo $proses['bukti_transfer']; ?>"><img src="../foto_bukti_pembayaran/<?php echo $proses['bukti_transfer']; ?>" class="img-responsive" width="100"></a></td>
                        </tr>
                        <?php
                    }
                    ?>
                </table>
                <form method="post">
                    <center>
                        <button name="submit" class="btn btn-primary">Konfirmasi</button>
                    <a onclick="history.go(-1);return false;" class="btn btn-danger">Cancel</a>
                    </center>
                </form>
                <?php 
                    if (isset($_POST['submit'])) {
                        $con->query("UPDATE list_booking SET status_dp='Lunas' WHERE id_booking='$id_booking'");

                        echo "<script>alert('Proses berhasil. Data telah disimpan');window.location='../admin/index.php?page=viewlistbooking'</script>";
                    }
                 ?>
            </div>
        </div>
    </div>
</div>
</div>