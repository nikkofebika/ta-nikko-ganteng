<?php
$id = $_GET['id_lapangan'];
$r = $con->query("SELECT * FROM lapangan WHERE id_lapangan = '$id'");
foreach ($r as $rr) { ?>
	<style type="text/css">
		.select2-container{
			width: 100% !important;
		}
		.select2-container--classic .select2-search--inline .select2-search__field{
			width: 100% !important;
		}

	</style>
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="index.php?page=home">
					<em class="fa fa-home"></em>
				</a></li>
				<li>
					<a href="index.php?page=viewlapangan">Lapangan</a>
				</li>
				<li class="active">Edit Lapangan</li>
			</ol>
		</div><!--/.row-->

		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Master Lapangan</h1>
			</div>
		</div><!--/.row-->

		<div class="panel panel-default">
			<!-- <div class="panel-heading">Input Here</div> -->
			<div class="panel-body">
				<div class="col-md-12">
					<form role="form" action="../controller/act_updatelapangan.php" method="POST" enctype="multipart/form-data">
						<div class="form-group">
							<label>Nama Lapangan</label>
							<input class="form-control" type="text" value="<?php echo $rr['nama_lapangan'];?>" placeholder="Nama Lapangan" name="nama_lapangan">
							<input type="hidden" value="<?php echo $rr['id_lapangan'];?>" name="id_lapangan">
						</div>
						<div class="form-group">
							<label>Size Lapangan</label>
							<select class="form-control" name="size_lapangan">
								<option value="large"<?php if($rr['size_lapangan'] == "large"){echo "selected";}?>>Large</option>
								<option value="small"<?php if($rr['size_lapangan'] == "small"){echo "selected";}?>>Small</option>
							</select>
						</div>
						<div class="form-group">
							<label>Ubah Foto</label>
							<img src="../foto/<?php echo $rr['picture']; ?>" class="img-responsive img-circle" width="100"><br>
							<input type="file" name="fotolp"></input>
						</div>
						<div class="form-group">
							<label>Harga</label>
							<input class="form-control" placeholder="Harga" type="number" value="<?php echo $rr['harga'];?>" name="harga" required>
						</div>
						<div class="form-group">
							<label>Fasilitas</label>
							<select class="js-example-basic-multiple" name="fasilitas[]" multiple="multiple">
								<?php 
									$f = explode(',', $rr['fasilitas']);
									foreach ($f as $fas) { ?>
								<option selected><?php echo $fas ?></option>
							<?php } ?>
							</select>
						</div>
						<div class="form-group">
							<label>Keterangan</label>
							<textarea rows="5" class="form-control" placeholder="Keterangan" name="keterangan" required><?php echo $rr['keterangan'];?></textarea>
						</div>
						<button type="submit" class="btn btn-primary">Update</button>
						<a onclick="history.go(-1);return false;" class="btn btn-danger">Back</a>
					</form>
				</div>
			</div>
		</div>
		<?php
	} ?>
	<script type="text/javascript">
		$(document).ready(function() {
			$('.js-example-basic-multiple').select2({
				placeholder: "Isi fasilitas lapangan disini",
				tags: true,
				theme: "classic",
				allowClear: true
			});
		});
	</script>