<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
	<div class="row">
		<ol class="breadcrumb">
			<li><a href="index.php?page=home">
				<em class="fa fa-home"></em>
			</a></li>
			<li class="active">customer</li>
		</ol>
	</div><!--/.row-->

	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Master Customer</h1>
		</div>
	</div><!--/.row-->

	<div class="panel panel-default">
    	<div class="panel-heading"><a href="index.php?page=savecustomer" class="btn btn-primary">Tambah</a></div>
    	<div class="panel-body">
    		<div class="col-md-12">
    			<div class="table-responsive">
                    <table class="table table-striped">
                        <tr>
                            <th>No</th>
                            <th>ID</th>
                            <th>Nama Lengkap</th>
    						<th>Email</th>
                            <th>No Telepon</th>
                            <th>Alamat</th>
                            <th>Actions</th>
                        </tr>
                        <?php
                        $no=1;
                        $ambil = $con->query("SELECT * FROM customer");
                        while ($customer = $ambil->fetch_assoc()) {
                            ?>
                            <tr>
                                <td><?php echo $no++; ?></td>
                                <td><?php echo $customer['id_customer']; ?></td>
                                <td><?php echo $customer['nama_lengkap']; ?></td>
                                <td><?php echo $customer['email']; ?></td>
                                <td><?php echo $customer['no_telp']; ?></td>
                                <td><?php echo $customer['alamat']; ?></td>
                                <td><a href="../admin/index.php?page=editcustomer&id_customer=<?php echo $customer['id_customer'];?>" class="btn btn-sm btn-success">Edit</a> <a href="../controller/act_delcustomer.php?id_customer=<?php echo $customer['id_customer'];?>" class="btn btn-sm btn-danger" onclick="return confirm('Yakin ingin hapus data ?')"> Hapus</a></td>
                            </tr>
                            <?php
                        }
                        ?>
                    </table>
                </div>
    		</div>
    	</div>
    </div>
</div>