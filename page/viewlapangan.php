<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
	<div class="row">
		<ol class="breadcrumb">
			<li><a href="index.php?page=home">
				<em class="fa fa-home"></em>
			</a></li>
			<li class="active">Lapangan</li>
		</ol>
	</div><!--/.row-->

	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Master Lapangan</h1>
		</div>
	</div><!--/.row-->

	<div class="panel panel-default">
	<div class="panel-heading"><a href="index.php?page=savelapangan" class="btn btn-primary">Tambah</a></div>
	<div class="panel-body">
		<div class="col-md-12">
			<div class="table-responsive">
                <table class="table table-striped">
                    <tr>
                        <th>ID Lapangan</th>
						<th>Nama Lapangan</th>
                        <th>Size Lapangan</th>
                        <th>Harga</th>
						<th>Keterangan</th>
						<th>Picture</th>
                        <th>Actions</th>
                    </tr>
                    <?php
                    $r = $con->query("SELECT * FROM lapangan");
                    while ($rr = $r->fetch_array()) {
                        ?>
                        <tr>
                            <td><?php echo $rr['id_lapangan'];?></td>
							<td><?php echo $rr['nama_lapangan'];?></td>
							<td><?php echo $rr['size_lapangan'];?></td>
                            <td><?php echo $rr['harga'];?></td>
							<td><?php echo $rr['keterangan'];?></td>
							<td><a href="../foto/<?php echo $rr['picture']; ?>"><img src="../foto/<?php echo $rr['picture']; ?>" class="img-responsive" width="50"></a></td>
                            <td><a href="../admin/index.php?page=editlapangan&id_lapangan=<?php echo $rr['id_lapangan'];?>" class="btn btn-sm btn-success">Edit</a> <a href="../controller/act_dellapangan.php?id_lapangan=<?php echo $rr['id_lapangan'];?>" class="btn btn-sm btn-danger" onclick="return confirm('Yakin ingin hapus data ?')"> Hapus</a></td>
                        </tr>
                        <?php
                    }
                    ?>
                </table>
            </div>
		</div>
	</div>
</div>