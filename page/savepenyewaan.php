<?php
$carikode = $con->query("SELECT MAX(id_penyewaan) FROM penyewaan");
$datakode = mysqli_fetch_array($carikode);
	if ($datakode) {
		$nilaikode = substr($datakode[0], 2);
   		$kode = (int) $nilaikode;
   		$kode = $kode + 1;
   		$kode_otomatis = "PM".str_pad($kode, 3, "0", STR_PAD_LEFT);
   	} else {
   		$kode_otomatis = "PM001";
  	}
?>

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
	<div class="row">
		<ol class="breadcrumb">
			<li><a href="#">
				<em class="fa fa-home"></em>
			</a></li>
			<li class="active">Pemesanan</li>
		</ol>
	</div><!--/.row-->

	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Pemesanan</h1>
		</div>
	</div><!--/.row-->

	<div class="panel panel-default">
	<div class="panel-heading">Input Here</div>
	<div class="panel-body">
		<form role="form" action="controler/act_savepenyewaan.php" method="POST" id="formpenyewaan">
			<div class="form-group col-md-12">
				<label>ID Pemesanan</label>
				<input class="form-control " placeholder="ID Reservation" type="text" name="idpenyewaan" value="<?php echo "$kode_otomatis"; ?>" readonly required>
			</div>
			<div class="form-group col-md-3">
				<label>ID Customer</label>
				<input class="form-control txtNikCustomer" placeholder="ID Customer" type="text" name="idcustomer" id="idcustomer" required>
			</div>
			<div class="form-group col-md-9">
				<label>Nama Customer</label>
				<input class="form-control" placeholder="Nama Customer" type="text" name="namacustomer" id="namacustomer" required readonly>
			</div>
			<div class="form-group col-md-9">
				<label>Alamat</label>
				<input class="form-control" placeholder="Alamat" type="text" name="alamat" id="alamat" required readonly>
			</div>
			<div class="form-group col-md-2">
				<label>Email</label>
				<input class="form-control txtCheckout" type="email" name="email" required>
			</div>
			<div class="form-group col-md-2">
				<label>ID Lapangan</label>
				<input class="form-control txtIdRoom" placeholder="ID Lapangan" type="text" name="idlapangan" id="idlapangan"  required>
			</div>
			<div class="form-group col-md-2">
				<label>Size Lapangan</label>
				<input class="form-control" placeholder="Size Lapangan" type="text" name="sizelapangan" id="sizelapangan" required readonly>
			</div>
			<div class="form-group col-md-2">
				<label>Harga</label>
				<input class="form-control" placeholder="Harga Lapangan" type="text" name="hargalapangan" id="hargalapangan" required readonly>
			</div>
			<div class="form-group col-md-6">
				<label>Tanggal Sewa</label>
				<input class="form-control" placeholder="Tanggal Sewa" type="text" name="tglsewa" id="tglsewa" required readonly>
			</div>
			<div class="form-group col-md-6">
				<label>Jam Mulai</label>
				<input class="form-control" placeholder="Jam Mulai" type="text" name="jammulai" id="jammulai" required readonly>
			</div>
			<div class="form-group col-md-6">
				<label>Jam Selesai</label>
				<input class="form-control" placeholder="Jam Mulai" type="text" name="jamselesai" id="jamselesai" required readonly>
			</div>
			<div class="form-group col-md-2">
				<label>Total Harga</label>
				<input class="form-control" placeholder="Total Harga" type="text" name="totalharga" id="totalharga" required readonly>
			</div>
			<div class="form-group col-md-12">
				<label>Keterangan</label>
				<textarea class="form-control" rows="2" name="keterangan" id="keterangan" maxlength="50" required readonly></textarea>
			</div>
			<div class="col-md-12">
				<button type="submit" class="btn btn-primary">Save</button>
				<a onclick="history.go(-1);return false;" class="btn btn-danger">Back</a>
			</div>
		</form>
	</div>
</div>