<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#myPage"><img src="foto/logo-lap-futsal.jpg" alt="New York" width="60" height="auto"></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#myPage">HOME</a></li>
        <li><a href="#tour">BOOKING</a></li>

        <?php if (isset($_SESSION['pelanggan'])): ?>
          <li><a href="riwayatbooking.php">RIWAYAT BOOKING</a></li>
          <li><a href="logout.php">LOGOUT</a></li>
          <!-- jika tidak ada session pelanggan -->
          <?php else: ?>
            <li><a data-toggle="modal" data-target="#login">LOGIN</a></li>
            <li><a data-toggle="modal" data-target="#register">REGISTER</a></li>
          <?php endif ?>
        </ul>
      </div>
    </div>
  </nav>