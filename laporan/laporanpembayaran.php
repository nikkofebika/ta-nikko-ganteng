<?php
/** $query = "SELECT * FROM customer ORDER BY id_customer";
$sql = mysql_query ($query);
$data = array();
while ($row = mysql_fetch_assoc($sql)) {
	array_push($data, $row);
}
 
#setting judul laporan dan header tabel
$judul = "LAPORAN DATA CUSTOMER";
$header = array(
		array("label"=>"ID CUSTOMER", "length"=>30, "align"=>"L"),
		array("label"=>"NAMA CUSTOMER", "length"=>50, "align"=>"L"),
		array("label"=>"Email", "length"=>50, "align"=>"L"),
		array("label"=>"ALAMAT CUSTOMER", "length"=>80, "align"=>"L"),
		array("label"=>"ALAMAT CUSTOMER", "length"=>80, "align"=>"L"),
		
		array("label"=>"TGL LAHIR", "length"=>30, "align"=>"L")
	);
 
#sertakan library FPDF dan bentuk objek
require_once ("laporan/fpdf.php");
$pdf = new FPDF();
$pdf->AddPage();
 
#tampilkan judul laporan
$pdf->SetFont('Arial','B','16');
$pdf->Cell(0,20, $judul, '0', 1, 'C');
 
#buat header tabel
$pdf->SetFont('Arial','','10');
$pdf->SetFillColor(255,0,0);
$pdf->SetTextColor(255);
$pdf->SetDrawColor(128,0,0);
foreach ($header as $kolom) {
	$pdf->Cell($kolom['length'], 5, $kolom['label'], 1, '0', $kolom['align'], true);
}
$pdf->Ln();
 
#tampilkan data tabelnya
$pdf->SetFillColor(224,235,255);
$pdf->SetTextColor(0);
$pdf->SetFont('');
$fill=false;
foreach ($data as $baris) {
	$i = 0;
	foreach ($baris as $cell) {
		$pdf->Cell($header[$i]['length'], 5, $cell, 1, '0', $kolom['align'], $fill);
		$i++;
	}
	$fill = !$fill;
	$pdf->Ln();
}
 
#output file PDF
$pdf->Output(); **/
?>
<?php
include ('../include/config.php');
session_start();
// echo "<pre>";
// print_r($_POST);
// echo "</pre>";die;
// Koneksi library FPDF
$printby = $_SESSION['username'];
$date = DATE('d-m-Y');
$tgl_mulai = $_POST['tgl_mulai'];
$tgl_akhir = $_POST['tgl_akhir'];
require('../laporan/fpdf.php');
// Setting halaman PDF
$pdf = new FPDF('l','mm','A4');
// Menambah halaman baru
$pdf->AddPage();
// Setting jenis font
$pdf->SetFont('Arial','B',16);
// Membuat string
$pdf->Cell(280,7,'Laporan Data Pembayaran',0,1,'C');
$pdf->SetFont('Arial','B',9);
// $pdf->Cell(280,7,'Jl. Magnolia Raya, Sukamulya, Cikupa, Tangerang, Banten 15710',0,1,'C');
$pdf->SetFont('Arial','B',8);
// $pdf->Cell(250,7,'PRINT BY '.$printby.'',0,1,'L');
$pdf->Cell(250,5,'Tanggal Cetak '.$date.'',0,2,'L');
// Setting spasi kebawah supaya tidak rapat
$pdf->Cell(10,7,'',0,1);

$pdf->SetFont('Arial','B',10);
$pdf->Cell(20,6,'ID Pembayaran',1,0);
$pdf->Cell(20,6,'ID Booking',1,0);
$pdf->Cell(20,6,'Nama Customer',1,0);
$pdf->Cell(20,6,'Tgl Pembayaran',1,0);
$pdf->Cell(20,6,'Tgl Main',1,0);
$pdf->Cell(20,6,'Total Pembayaran',1,0);
 
$pdf->SetFont('Arial','',10);
$query = $con->query("SELECT * FROM pembayaran JOIN list_booking ON pembayaran.id_booking=list_booking.id_booking JOIN customer ON list_booking.id_customer=customer.id_customer WHERE date(tgl_pembayaran) between DATE('$tgl_mulai') AND DATE('$tgl_akhir') ORDER BY tgl_pembayaran ASC");
foreach ($query as $row) {
    $pdf->Cell(20,6,$row['id_pembayaran'],1,0);
    $pdf->Cell(20,6,$row['id_booking'],1,0);
    $pdf->Cell(20,6,$row['nama_lengkap'],1,0);
    $pdf->Cell(20,6,$row['tgl_pembayaran'],1,0);
    $pdf->Cell(20,6,$row['tgl_main'],1,0);
	$pdf->Cell(20,6,$row['total_pembayaran'],1,0);
}
$pdf->SetFont('Arial','',12);
$pdf->Cell(475,18,'Tangerang, '.$date.'',0,1,'C');
$pdf->SetFont('Arial','',12);
$pdf->Cell(475,20,''.$printby.'',0,1,'C');
$pdf->Output();
?>