<?php
include ('../include/config.php');
session_start();
$printby = $_SESSION['username'];
$date = DATE('d-m-Y');
$type = $_POST['cmbtype'];
$tg1 = $_POST['tg1'];
$th1 = $_POST['th1'];
$w = $th1."-".$tg1;


require('fpdf.php');

$pdf = new FPDF('l','mm','A5');

$pdf->AddPage();

$pdf->SetFont('Arial','B',16);
 
$pdf->Cell(190,7,'YUDI HOTEL',0,1,'C');
$pdf->SetFont('Arial','B',12);
$pdf->Cell(190,7,'REPORT ACCORDING TYPE ROOM AND PERIODE',0,1,'C');
$pdf->SetFont('Arial','B',8);
$pdf->Cell(190,7,'PRINT BY '.$printby.'',0,1,'L');
$pdf->Cell(190,7,'PRINT DATE '.$date.'',0,2,'L');
 

$pdf->Cell(10,7,'',0,1);
 
$pdf->SetFont('Arial','B',10);
$pdf->Cell(20,6,'ID',1,0);
$pdf->Cell(40,6,'Check IN',1,0);
$pdf->Cell(30,6,'Check OUT',1,0);
$pdf->Cell(30,6,'Customer',1,0);
$pdf->Cell(40,6,'Room',1,0);
$pdf->Cell(30,6,'Grandtotal',1,1);
 
$pdf->SetFont('Arial','',10);
 
$d = $con->query("SELECT reservations.id_reservation, reservations.date_checkin, reservations.date_checkout, customers.name_customer AS namacustomer, reservations.id_room, reservations.grandtotal FROM (rooms INNER JOIN (customers INNER JOIN reservations ON customers.nik_customer = reservations.nik_customer) ON rooms.id_room = reservations.id_room) INNER JOIN types ON rooms.id_type = types.id_type WHERE types.id_type = '$type' AND month(reservations.date_checkin) = '$tg1' AND year(reservations.date_checkin) = '$th1'");
    foreach ($d as $dd) {
    $pdf->Cell(20,6,$dd['id_reservation'],1,0);
	$pdf->Cell(40,6,$dd['date_checkin'],1,0);
    $pdf->Cell(30,6,$dd['date_checkout'],1,0);
    $pdf->Cell(30,6,$dd['namacustomer'],1,0);
    $pdf->Cell(40,6,$dd['id_room'],1,0); 
	$uang = number_format($dd['grandtotal']);
	$pdf->Cell(30,6,$uang,1,1); 
}
 
$pdf->Output();
?>