<?php
/** $query = "SELECT * FROM customer ORDER BY id_customer";
$sql = mysql_query ($query);
$data = array();
while ($row = mysql_fetch_assoc($sql)) {
	array_push($data, $row);
}
 
#setting judul laporan dan header tabel
$judul = "LAPORAN DATA CUSTOMER";
$header = array(
		array("label"=>"ID CUSTOMER", "length"=>30, "align"=>"L"),
		array("label"=>"NAMA CUSTOMER", "length"=>50, "align"=>"L"),
		array("label"=>"Email", "length"=>50, "align"=>"L"),
		array("label"=>"ALAMAT CUSTOMER", "length"=>80, "align"=>"L"),
		array("label"=>"ALAMAT CUSTOMER", "length"=>80, "align"=>"L"),
		
		array("label"=>"TGL LAHIR", "length"=>30, "align"=>"L")
	);
 
#sertakan library FPDF dan bentuk objek
require_once ("laporan/fpdf.php");
$pdf = new FPDF();
$pdf->AddPage();
 
#tampilkan judul laporan
$pdf->SetFont('Arial','B','16');
$pdf->Cell(0,20, $judul, '0', 1, 'C');
 
#buat header tabel
$pdf->SetFont('Arial','','10');
$pdf->SetFillColor(255,0,0);
$pdf->SetTextColor(255);
$pdf->SetDrawColor(128,0,0);
foreach ($header as $kolom) {
	$pdf->Cell($kolom['length'], 5, $kolom['label'], 1, '0', $kolom['align'], true);
}
$pdf->Ln();
 
#tampilkan data tabelnya
$pdf->SetFillColor(224,235,255);
$pdf->SetTextColor(0);
$pdf->SetFont('');
$fill=false;
foreach ($data as $baris) {
	$i = 0;
	foreach ($baris as $cell) {
		$pdf->Cell($header[$i]['length'], 5, $cell, 1, '0', $kolom['align'], $fill);
		$i++;
	}
	$fill = !$fill;
	$pdf->Ln();
}
 
#output file PDF
$pdf->Output(); **/
?>
<?php
include ('../include/config.php');
session_start();
$printby = $_SESSION['username'];
$date = DATE('d-m-Y');
// Koneksi library FPDF
require('../laporan/fpdf.php');
// Setting halaman PDF
$pdf = new FPDF('l','mm','A5');
// Menambah halaman baru
$pdf->AddPage();
// Setting jenis font
$pdf->SetFont('Arial','B',16);
// Membuat string
$pdf->Cell(190,7,'Laporan Data Customer',0,1,'C');
$pdf->SetFont('Arial','B',9);
// $pdf->Cell(190,7,'Jl. Magnolia Raya, Sukamulya, Cikupa, Tangerang, Banten 15710',0,1,'C');
$pdf->SetFont('Arial','B',8);
// $pdf->Cell(250,4,'PRINT BY '.$printby.'',0,1,'L');
$pdf->Cell(250,3,'Tanggal Cetak '.$date.'',0,2,'L');
// Setting spasi kebawah supaya tidak rapat
$pdf->Cell(10,2,'',0,1);

$pdf->SetFont('Arial','B',10);
$pdf->Cell(20,6,'ID',1,0);
$pdf->Cell(50,6,'NAMA LENGKAP',1,0);
$pdf->Cell(45,6,'EMAIL',1,0);
$pdf->Cell(30,6,'NO TELEPON',1,0);
$pdf->Cell(53,6,'ALAMAT',1,1);
 
$pdf->SetFont('Arial','',10);
$query = $con->query("SELECT id_customer, nama_lengkap, email, no_telp, alamat FROM customer");
foreach ($query as $row) {
    $pdf->Cell(20,6,$row['id_customer'],1,0);
    $pdf->Cell(50,6,$row['nama_lengkap'],1,0);
    $pdf->Cell(45,6,$row['email'],1,0);
    $pdf->Cell(30,6,$row['no_telp'],1,0);
    $pdf->Cell(53,6,$row['alamat'],1,1);
}

$pdf->Output();
?>