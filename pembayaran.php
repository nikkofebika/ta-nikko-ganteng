<?php 
session_start();
include 'include/config.php';

$id_booking = $_GET['id_booking'];
$id_customer = $_SESSION['pelanggan']['id_customer'];

if (empty($_SESSION['pelanggan']) || !isset($_SESSION['pelanggan'])) {
  echo "<script>alert('Anda Belum Login. Silahkan Login Dulu');</script>";
  echo "<script>location='index.php';</script>";
}

//QUERY UNTUK MENDAPATKAN DATA LIST BOOKING
$q = $con->query("SELECT * FROM list_booking WHERE id_booking='$id_booking'");
$nota = $q->fetch_assoc();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Theme Made By www.w3schools.com - No Copyright -->
  <title>BINTANG FUTSAL</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link href="css/font.css" rel="stylesheet" type="text/css">
  <script src="js/jquery-1.11.1.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <style>
    body {
      font: 400 15px/1.8 Lato, sans-serif;
      color: #777;
    }
    h3, h4 {
      margin: 10px 0 30px 0;
      letter-spacing: 10px;      
      font-size: 20px;
      color: #111;
    }
    .container {
      padding: 80px 120px;
    }
    .person {
      border: 10px solid transparent;
      margin-bottom: 25px;
      width: 80%;
      height: 80%;
      opacity: 0.7;
    }
    .person:hover {
      border-color: #f1f1f1;
    }
    .carousel-inner img {
      -webkit-filter: grayscale(90%);
      filter: grayscale(90%); /* make all photos black and white */ 
      width: 100%; /* Set width to 100% */
      margin: auto;
    }
    .carousel-caption h3 {
      color: #fff !important;
    }
    @media (max-width: 600px) {
      .carousel-caption {
        display: none; /* Hide the carousel text when the screen is less than 600 pixels wide */
      }
    }
    .bg-1 {
      background: #2d2d30;
      color: #bdbdbd;
    }
    .bg-1 h3 {color: #fff;}
    .bg-1 p {font-style: italic;}
    .list-group-item:first-child {
      border-top-right-radius: 0;
      border-top-left-radius: 0;
    }
    .list-group-item:last-child {
      border-bottom-right-radius: 0;
      border-bottom-left-radius: 0;
    }
    .thumbnail {
      padding: 0 0 15px 0;
      border: none;
      border-radius: 0;
    }
    .thumbnail p {
      margin-top: 15px;
      color: #555;
    }

    .modal-header, h4, .close {
      background-color: #333;
      color: #fff !important;
      text-align: center;
      font-size: 30px;
    }
    .modal-body {
      padding: 40px 50px;
    }
    .nav-tabs li a {
      color: #777;
    }
    #googleMap {
      width: 100%;
      height: 400px;
      -webkit-filter: grayscale(100%);
      filter: grayscale(100%);
    }  
    .navbar {
      font-family: Montserrat, sans-serif;
      margin-bottom: 0;
      background-color: #2d2d30;
      border: 0;
      font-size: 11px !important;
      letter-spacing: 4px;
      opacity: 0.9;
    }
    .navbar li a, .navbar .navbar-brand { 
      color: #d5d5d5 !important;
    }
    .navbar-nav li a:hover {
      color: #fff !important;
    }
    .navbar-nav li.active a {
      color: #fff !important;
      background-color: #29292c !important;
    }
    .navbar-default .navbar-toggle {
      border-color: transparent;
    }
    .open .dropdown-toggle {
      color: #fff;
      background-color: #555 !important;
    }
    .dropdown-menu li a {
      color: #000 !important;
    }
    .dropdown-menu li a:hover {
      background-color: red !important;
    }
    footer {
      background-color: #2d2d30;
      color: #f5f5f5;
      padding: 32px;
    }
    footer a {
      color: #f5f5f5;
    }
    footer a:hover {
      color: #777;
      text-decoration: none;
    }  
    .form-control {
      border-radius: 0;
    }
    textarea {
      resize: none;
    }
  </style>
</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="50">
  <nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>                        
        </button>
        <a class="navbar-brand" href="#myPage"><img src="foto/logo-lap-futsal.jpg" alt="New York" width="60" height="auto"></a>
      </div>
      <div class="collapse navbar-collapse" id="myNavbar">
        <ul class="nav navbar-nav navbar-right">
          <li><a href="index.php">HOME</a></li>

          <?php if (isset($_SESSION['pelanggan'])): ?>
            <li><a href="riwayatbooking.php">RIWAYAT BOOKING</a></li>
            <li><a href="logout.php">LOGOUT</a></li>
            <!-- jika tidak ada session pelanggan -->
            <?php else: ?>
              <li><a data-toggle="modal" data-target="#login">LOGIN</a></li>
              <li><a data-toggle="modal" data-target="#register">REGISTER</a></li>
            <?php endif ?>


          </ul>
        </div>
      </div>
    </nav>
    <section class="lapangan">
      <div class="container">
        <table class="table table-bordered table-striped table-hover">
          <h2>Konfirmasi Pembayaran</h2>
          <p>Kirim bukti pembayaran anda disini</p>
          <div class="alert alert-info">Total tagihan anda Rp. <strong><?php echo number_format($nota['dp']) ?></strong></div>

          <form method="post" enctype="multipart/form-data">
            <div class="form-group">
              <label>Nama Penyetor</label>
              <input type="text" name="nama" required class="form-control">
            </div>
            <div class="form-group">
              <label>Bank</label>
              <select name="bank" required class="form-control" required>
                <?php 
                $bank = $con->query("SELECT * FROM bank ORDER BY nama_bank asc");
                foreach ($bank as $bank) { ?>
                  <option value="<?php echo $bank['id_bank'] ?>"><?php echo $bank['nama_bank'] ?></option>
                <?php } ?>
              </select>
              <!-- <input type="text" name="bank" required class="form-control"> -->
            </div>
            <div class="form-group">
              <label>Foto Bukti</label>
              <input type="file" name="bukti_pembayaran" required class="form-control">
              <div class="text-danger">File JPG maksimal 2MB</div>
            </div>
            <button class="btn btn-primary" name="kirim">Kirim</button>
          </form>

          <!-- PROSES SIMPAN PEMBAYARAN DP -->
          <?php 
          if (isset($_POST['kirim'])) {

            // echo "<pre>";
            // print_r($_POST);
            // echo "</pre>"."<br>";die;

            $sql = $con->query("SELECT RIGHT(id_pembayaran, 2) as kode FROM pembayaran ORDER BY id_pembayaran DESC LIMIT 1");

            $hitung = mysqli_num_rows($sql);

            if ($hitung > 0) {
              $q = $sql->fetch_assoc();
              $kode = $q['kode']+1;
              // echo $kode."<br>";
            }else{
              $kode = 1;
            }
            $code = "PB".date('dmY').str_pad($kode, 2,"0", STR_PAD_LEFT);
            // echo $code;die;

            $nama = $_POST['nama'];
            $bank = $_POST['bank'];
            // echo $bank;die;
            $jumlah_pembayaran = $nota['dp'];
            $total_pembayaran = $nota['total_harga'];
            $tanggal = date("Y-m-d");

            $nama_foto      = $_FILES['bukti_pembayaran']['name'];
            $lokasi_foto    = $_FILES['bukti_pembayaran']['tmp_name'];
        //penanganan apabila nama file yang diupload sama. misal joko upload nama filenya bukti.jpg . anton bukti.jpg juga. maka penangannya adalah dengan di rename pake waktu biar ga akan pernah sama
            $bukti_pembayaran = date("YmdHis").$nama_foto;
            move_uploaded_file($lokasi_foto, "foto_bukti_pembayaran/$bukti_pembayaran");

        //simpan ke table pembayaran
                //kodinglama $con->query("INSERT INTO pembayaran_dp (id_booking, nama_penyetor, bank, jumlah_pembayaran, tgl_pembayaran, bukti_pembayaran) VALUES ('$id_booking','$nama','$bank','$jumlah_pembayaran','$tanggal','$bukti_pembayaran')");

                //koding baru
            // echo "INSERT INTO pembayaran (id_pembayaran,id_booking, nama_penyetor, bank, uang_muka, total_pembayaran, tgl_transfer, bukti_transfer) VALUES ('$code','$id_booking','$nama','$bank','$jumlah_pembayaran', '$total_pembayaran','$tanggal','$bukti_pembayaran')";die;
            $con->query("INSERT INTO pembayaran (id_pembayaran,id_booking, nama_penyetor, id_bank, uang_muka, total_pembayaran, tgl_transfer, bukti_transfer) VALUES ('$code','$id_booking','$nama','$bank','$jumlah_pembayaran', '$total_pembayaran','$tanggal','$bukti_pembayaran')");

        //setelah kirim pembayaran update dong data pembeliannya(tabel pembelian) dari pending menjadi sudah kirim pembayaran
            $con->query("UPDATE list_booking SET status_dp='Sedang Diproses' WHERE id_booking='$id_booking' ");

            echo "<script>alert('Terimakasih pembayaran berhasil. Data akan segera kami proses');</script>";
            echo "<script>location='riwayatbooking.php?id_customer=$id_customer ';</script>";
          }

          ?>
          <!-- /PROSES SIMPAN PEMBAYARAN DP -->

        </table>
      </div>
    </section>
  </body>
  </html>
